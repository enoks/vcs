<?php
/**
 * siehste functions and definitions
 *
 * @package siehste
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
add_action( 'after_setup_theme', 'siehste__after_setup_theme' );
function siehste__after_setup_theme() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on siehste, use a find and replace
	 * to change 'siehste' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'siehste', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
//    add_image_size( 'post-thumbnail', 740, 416, true ); // 16:9

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'siehste' ),
		'footer' => __( 'Footer Menu', 'siehste' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'status'
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'siehste_custom_background_args', array(
		'default-color' => 'f2f2f2',
		'default-image' => '',
	) ) );
}

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
add_action( 'widgets_init', 'siehste__widgets_init' );
function siehste__widgets_init() {
	foreach ( array( 'Sidebar', 'Ads' ) as $i => $sidebar ) {
		register_sidebar( array(
			'name'          => __( $sidebar, 'siehste' ),
			'id'            => 'sidebar-' . ++$i,
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}

    // remove unneeded widgets
    unregister_widget( 'WP_Widget_Calendar' );
    unregister_widget( 'WP_Widget_Pages' );
    unregister_widget( 'WP_Widget_Tag_Cloud' );
    unregister_widget( 'WP_Widget_Meta' );
    unregister_widget( 'WP_Widget_Recent_Posts' );
    unregister_widget( 'WP_Widget_Categories' );
    unregister_widget( 'WP_Widget_RSS' );
    unregister_widget( 'WP_Widget_Archives' );
    unregister_widget( 'WP_Nav_Menu_Widget' );
}

// enable shortcodes for sidebar widgets
add_filter( 'widget_title', 'do_shortcode' );
add_filter( 'widget_text', 'do_shortcode' );

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'siehste_enqueue_scripts', 100 );
function siehste_enqueue_scripts() {
	wp_enqueue_style( 'open-sans' ); // Open Sans font
	wp_enqueue_style( 'siehste-style', get_stylesheet_uri(), array(), '20191125' );

	wp_enqueue_script( 'siehste-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

    // tiled gallery + lightbox
    wp_enqueue_script( 'tiled-gallery', get_template_directory_uri() . '/js/gallery.js', '20200817' );

    wp_enqueue_script( 'peekaboo', get_template_directory_uri() . '/js/peekaboo.js', array(), '1.2.1' );
    wp_enqueue_script( 'siehste-js', get_template_directory_uri() . '/js/siehste.js', array( 'peekaboo' ), '20200929' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// try to replace css and js files with minimized version
	// ... but not for dev environment.
	if ( strpos( $_SERVER['SERVER_NAME'], 'dev.' ) !== 0 ) {
//		require_once( dirname( __FILE__ ) . '/../../../wp-admin/includes/file.php' );
		$home_path = ( function_exists( 'get_home_path' ) ? get_home_path() : $_SERVER['DOCUMENT_ROOT'] );

		$home_url = preg_quote( get_home_url(), '/' );

		// got for all css and js files
		foreach ( array( wp_styles(), wp_scripts() ) as $files ) {
			foreach ( $files->registered as $file ) {
				// not minimized yet
				if ( !is_string( $file->src ) || !preg_match( '/(?<!\.min)\.(css|js)$/', $file->src ) ) continue;
				// is local file
				if ( !preg_match( '/^(\/|' . $home_url . ')/', $file->src ) ) continue;

				// relative (from DOCUMENT_ROOT) path to minimized version
				$minimized = preg_replace( array( '/^' . $home_url . '/', '/\.(css|js)$/' ), array( '', ".min.$1" ), $file->src );

				// check if minimized file exists
				if ( file_exists( $home_path . $minimized ) ) {
					// ... and replace/insert it
					$file->src = $minimized;
				}
			}
		}
	}
}

/**
 * ...
 */
add_action( 'pre_get_posts', 'siehste__pre_get_posts' );
function siehste__pre_get_posts( $query ) {
    // remove category 'Termin' from homepage
    if ( $query->is_home() && $query->is_main_query() ) {
        $query->set( 'cat', '-3' );
    }

    // show scheduled posts for category 'Termin'
    if ( $query->get( 'cat' ) == 3 ) {
        $query->set( 'post_status', 'future' );
        $query->set( 'order', 'ASC' );
    }
}

/**
 * ...
 */
add_filter( 'the_posts', 'siehste__the_posts' );
function siehste__the_posts( $posts ) {
	global $wp_query, $wpdb;

	// show single future posts
	if( is_single() && $wp_query->post_count == 0 ){
		$posts = $wpdb->get_results( $wp_query->request );
	}

	return $posts;
};

/**
 * ...
 *
 * @param $fields
 * @return array
 */
add_filter( 'comment_form_default_fields', 'siehste__comment_form_default_fields' );
function siehste__comment_form_default_fields( $fields ) {
    $fields = array(
        'author' => array(
            'label' => __( 'Name' ),
            'type' => 'text',
            'required' => true,
            'placeholder' => __( 'Name' ),
//            'description' => __('Required.'),
        ),
        'email' => array(
            'label' => __( 'Email' ),
            'type' => 'text',
            'required' => true,
            'placeholder' => __( 'Email' ),
//            'description' => __('Required.') . ' ' . __( 'Your email address will not be published.' ),
        ),
        'url' => array(
            'label' => __( 'Website' ),
            'type' => 'text',
            'placeholder' => __( 'Website' ),
        ),
    ) + $fields;

    foreach ( $fields as $name => &$field ) {
        if ( is_string( $field ) ) continue;

        $field = '<p class="comment-form-' . $name . '"' . ( !empty($field['required']) ? ' required' : '' ) . '>'
            . '<label for="' . $name . '">' . $field['label'] . '</label>'
            . '<input id="' . $name . '" name="' . $name . '" type="' . $field['type'] . '" value=""' . ( !empty($field['required']) ? ' aria-required="true"' : '' ) . ' placeholder="' . $field['placeholder'] . ( !empty($field['required']) ? ' *' : '' ) . '" />'
            . ( !empty( $field['description'] ) ? '<span class="description">' . $field['description'] . '</span>' : '' )
            . '</p>';
    }

    return $fields;
}

/**
 * Redirect user after successful login.
 */
add_filter( 'login_redirect', 'siehste__login_redirect', 10, 3 );
function siehste__login_redirect( $redirect_to, $request, $user ) {
	global $user;

	// redirect subscribers to front page
	if ( isset( $user->roles ) && is_array( $user->roles ) && in_array( 'subscriber', $user->roles ) ) {
		$redirect_to = home_url();
	}

	return $redirect_to;
}

/**
 * ...
 *
 * @param $content
 * @return string
 */
add_filter( 'the_content', 'siehste__the_content' );
function siehste__the_content( $content ) {
    // Parse content for columns marker (<!--more-->) and wrap intro.
    if ( is_single() ) {
	    $post_thumbnail = siehste_post_thumbnail( 'large', false );

	    if ( count( $parts = preg_split( '/<span id="more-[0-9]+"><\/span>/', $content ) ) > 1 ) {
		    $parts[0] = '<div class="entry-teaser">' . $parts[0] . '</div>' .
		                $post_thumbnail;

		    $content = implode( "", $parts );
	    }
	    else $content = $post_thumbnail . $content;
    }

    return wpautop( $content );
}

/**
 * Alter share links.
 */
add_filter( 'share_link_facebook', 'siehste__share_link_facebook' );
function siehste__share_link_facebook( $link ) {
	if ( strpos( $_SERVER['HTTP_HOST'], 'dev.' ) !== false ) $share['query']['app_id'] = 825986400842914;

	return $link;
}

/**
 * Alter meta data.
 */
add_filter( 'share_meta', 'siehste__share_meta' );
function siehste__share_meta( $meta ) {
	if ( empty( $meta['image'] ) ) {
		$meta['image'] = get_bloginfo( 'url', 'display' ) . '/wp-content/themes/siehste/images/logo__share.jpg';
	}

	return $meta;
}

/**
 * WP Dashboard.
 */
add_action( 'wp_dashboard_setup', 'probatio__wp_dashboard_setup' );
function probatio__wp_dashboard_setup() {
  remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
  remove_action( 'welcome_panel', 'wp_welcome_panel' );
}

/**
 * Includes
 */

require get_template_directory() . '/inc/template-tags.php'; // custom template tags for this theme
require get_template_directory() . '/inc/customizer.php'; // customizer additions
require get_template_directory() . '/inc/attachment.php'; // extend attachment aka media uploader
require get_template_directory() . '/inc/widgets/widgets.php'; // siehste widgets
require get_template_directory() . '/inc/editor.php'; // siehste widgets
require get_template_directory() . '/inc/user.picture.php'; // user picture
// include shortcodes
foreach ( scandir( get_template_directory() . '/inc' ) as $file ) {
	if ( !preg_match( '/shortcode\..+\.php/', $file ) ) continue;
	require get_template_directory() . '/inc/' . $file;
}
//require get_template_directory() . '/inc/import.php'; // import old news
