<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package siehste
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">

      <nav id="footer-navigation" class="footer-navigation" role="navigation">
        <button class="menu-toggle" aria-controls="menu" aria-expanded="false"><?php _e( 'Footer Menu', 'siehste' ); ?></button>
        <?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?>
      </nav><!-- #site-navigation -->

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
