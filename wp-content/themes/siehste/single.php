<?php
/**
 * The template for displaying all single posts.
 *
 * @package siehste
 */

get_header(); ?>

<div id="primary" class="content-area">
  <main id="main" class="site-main" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

      <?php // original @see /wp-includes/general-template.php:get_template_part()
      $slug = 'template-parts/content';
      $name = get_post_type();
      do_action( "get_template_part_{$slug}", $slug, $name );

      $templates = array(
        'content-' . get_post_type() . '.php',
        'content-single.php',
        'content.php',
      );

      locate_template($templates, true, false); ?>

      <?php //the_post_navigation(); ?>

      <?php
      // If comments are open or we have at least one comment, load up the comment template
      if ( comments_open() || get_comments_number() ) :
        comments_template();
      endif;
      ?>

    <?php endwhile; // end of the loop. ?>

  </main><!-- #main -->

  <?php get_sidebar( 'content' ); ?>

</div><!-- #primary -->

<?php get_sidebar( 'ads' ); ?>
<?php get_footer(); ?>
