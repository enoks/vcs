<?php
/**
 * @package siehste
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<?php siehste_posted_on();
			edit_post_link(); ?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

    <?php share_links(); ?>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'siehste' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php siehste_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
