( function( window, document, undefined ) {

    document.addEventListener( "DOMContentLoaded", function() {

        // lazy load images
        peekaboo( '[data-src]', {
            callback: function() {
                // try to get src
                var source = this.getAttribute( 'data-src' );

                // 'load'
                if (source) {
                    // check if item is a gallery image
                    var isGalleryItem = this.parentNode;
                    while ( isGalleryItem && isGalleryItem.tagName != 'FIGURE' ) {
                        isGalleryItem = isGalleryItem.parentNode;
                    }

                    // gallery images inserted as background
                    if ( isGalleryItem ) this.style.backgroundImage = "url('" + source + "')";
                    // ... other as src
                    else this.setAttribute( 'src', source );

                    this.removeAttribute( 'data-src' );
                }
            }
        } );

        // open external links ALL in new window
        ( function() {

            var links = document.getElementsByTagName( 'a' );
            for ( var i = 0; i < links.length; i++ ) {
                // we don't override existing target
                if ( links[i].getAttribute( 'target' ) ) continue;

                // check for internal link
                if ( links[i].getAttribute( 'href' ).match( new RegExp( '^(\/|\\#|(https?:\/\/)' + location.host + ')' ) ) )
                    continue;

                // is external link ... target="_blank"
                links[i].setAttribute( 'target', '_blank' );
            }

        } )();

        // WPCF7
        // pre-select recipient
        ( function() {

            var recipient = location.search;
            if ( !recipient ) return;

            recipient = recipient.replace( new RegExp( '^\\?' ), '' );
            recipient = recipient.split( '&' );
            for ( var i = 0; i < recipient.length; i++ ) {
                recipient[i] = recipient[i].split( '=' );

                if ( recipient[i][0] != 'recipient' ) continue;

                recipient = decodeURIComponent( recipient[i][1] );
                break;
            }

            if ( typeof recipient != 'string' ) return;

            for ( i = 0; i < document.forms.length; i++ ) {
                if ( document.forms[i].className.indexOf( 'wpcf7-form' ) < 0 ) continue;

                var $recipient = document.forms[i];
                break;
            }

            if ( !$recipient ) return;

            $recipient = $recipient.getElementsByTagName( 'select' );
            for ( i = 0; i < $recipient.length; i++ ) {
                if ( $recipient[i].name != 'recipient' ) continue;

                var options = $recipient[i].getElementsByTagName( 'option' );
                for ( var j = 0; j < options.length; j++ ) {
                    if ( options[j].value.indexOf( recipient ) < 0 ) continue;

                    options[j].selected = true;
                    break;
                }

                break;
            }

        } )();

        // Google Maps
        ( function() {

            var $gMaps = document.getElementsByClassName( 'gm-canvas' );
            if ( !$gMaps.length ) return;

            try {
                window.gMapsCallback();
            }
            catch( e ) {
                var gMapsApi = document.createElement( 'script' );
                gMapsApi.setAttribute( 'type', 'text/javascript' );
                gMapsApi.setAttribute( 'src', 'https://maps.google.com/maps/api/js?key=AIzaSyCXzQa3dvRRu4fDrHH5jbUVuex7X-16MrQ&callback=gMapsCallback' );
                ( document.getElementsByTagName( 'head' )[0] || document.documentElement ).appendChild( gMapsApi );

                window.gMapsCallback = function() {
                    for (var i = 0; i < $gMaps.length; i++ ) {

                        ( function() {

                            var $gMap = $gMaps[i],
                                sAddress = $gMap.getElementsByTagName( 'address' )[0].innerHTML.replace( new RegExp( '(<([^>]+)>)', 'ig' ), '').split( "," ),
                                sInfo = $gMap.getElementsByClassName( 'info' )[0].innerHTML || sAddress.join( '<br />' ),
                                geocoder = new google.maps.Geocoder();

                            geocoder.geocode( { address: sAddress.join( ', ' ) }, function( results, status ) {
                                if ( status == google.maps.GeocoderStatus.OK ) {
                                    // map options
                                    var options = {
                                        center: results[0].geometry.location
                                        ,scrollwheel: false
                                        ,mapTypeId: google.maps.MapTypeId.ROADMAP
                                        ,zoom: 16
                                        ,disableDefaultUI: true

                                        ,mapTypeControl: true
                                        //,zoomControl: false
                                        //,streetViewControl: false
                                        //,scaleControl: true

                                    };

                                    // create map
                                    var map = new google.maps.Map( $gMap, options),
                                    // create marker
                                        marker = new google.maps.Marker( {
                                            map: map,
                                            position: results[0].geometry.location,
                                            title: sAddress.join( ', ' )
                                        }),
                                    // create info window
                                        info = new google.maps.InfoWindow( {
                                            content: sInfo
                                        } );

                                    marker.addListener( 'click', function() {
                                        info.open( map, marker );
                                    } );

                                    // ...
                                    google.maps.event.addListenerOnce( map, 'idle', function() {
                                        google.maps.event.trigger( map, 'resize' );
                                        map.setCenter(results[0].geometry.location);

                                        info.open( map, marker );
                                    } );
                                }

                                // remove loading class
                                $gMap.parentNode.className = $gMap.parentNode.className.replace( ' loading', '' );

                            } );

                        } )();
                    }
                };
            }
        } )();

        /**
         * GA
         * see https://www.dair-media.net/blog/google-analytics-mit-optin-implementieren/
         */

        (function() {
            var gaProperty = 'UA-67835317-1';

            var disableStr = 'ga-disable-' + gaProperty;
            if ( document.cookie.indexOf( disableStr + '=true' ) > -1 ) {
                window[disableStr] = true;
            }

            function loadGA() {
                window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
                ga('create', gaProperty, 'auto');
                ga('set', 'anonymizeIp', true);
                ga('send', 'pageview');
                var gascript = document.createElement("script" );
                gascript.async = true;
                gascript.src = "https://www.google-analytics.com/analytics.js";
                document.getElementsByTagName("head" )[0].appendChild( gascript, document.getElementsByTagName("head" )[0] );
            }

            // load GA immediately if cookies are allowed
            if ( typeof Sid !== 'undefined' && Sid.accepted ) loadGA();
            // ... otherwise wait till they are
            else document.body.addEventListener( 'sid_accepted', loadGA, false );

            window.gaOptout = function() {
                document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
                window[disableStr] = true;
            }
        })();

    }, false);

} )( this, this.document );
