/**
 * Horizontally tiled gallery.
 *
 * Licensed under the MIT license.
 * Copyright 2015 Stefan Käsche
 */

( function() {
    "user strict";

    document.addEventListener( "DOMContentLoaded", function() {

        // get all galleries
        var galleries = document.getElementsByClassName( 'gallery'),
            // define other vars
            images, columns,
            galleryWidth, rowHeight, tileWidth;

        // random row height
        function getRandomRowHeight() {
            return Math.floor( ( Math.random() * ( arguments[1] || 50 ) ) + ( arguments[0] || 100 ) );
        }

        // random tile width
        function getRandomTileWidth( min, max ) {
            return Math.floor( ( Math.random() * ( max - min ) ) + min );
        }

        // gallery width
        function resetGalleryWidth( unit ) {
            var data;

            if ( !unit || unit == '%' ) data = { width: 100, unit: '%'};
            else data = { width: parseInt( window.getComputedStyle( galleries[i] )['width'] ), unit: 'px'};

            data.origWidth = data.width;

            return data;
        }

        // loop through galleries
        for ( var i = 0; i < galleries.length; i++ ) {
            // get gallery items
            images = galleries[i].getElementsByClassName( 'gallery-item');
            // get WP defined columns
            columns = parseInt( galleries[i].className.match( new RegExp( 'gallery-columns-(\\d)' ) )[1] );

            // loop through gallery items
            for ( var j = 0; j < images.length; j++ ) {
                // new row
                if ( j % columns == 0 ) {
                    galleryWidth = resetGalleryWidth();
                    rowHeight = getRandomRowHeight();
                }

                // calculate width of image (tile)
                if ( j % columns < ( columns - 1 ) && j < ( images.length - 1 ) ) {
                    tileWidth = getRandomTileWidth( galleryWidth.origWidth / ( columns * 2 ), galleryWidth.width / 2 );

                    galleryWidth.width -= tileWidth;
                }
                // last image in row
                else tileWidth = galleryWidth.width;

                // resize item
                images[j].style.width = tileWidth + galleryWidth.unit;
                images[j].style.height = rowHeight + 'px';
            }
        }

    } );

})();

/**
 * Image lightbox.
 *
 * Licensed under the MIT license.
 * Copyright 2015 Stefan Käsche
 */

( function( window, document ) {
    "use strict";

    document.addEventListener( "DOMContentLoaded", function() {

        var lightbox, gallery = [], current = null, image = new Image(),
            navigation, prev, next,
            canvas, cursor = {}, cursorDimension = 51, context;

        // cursors
        canvas = document.createElement( "canvas" );
        canvas.width = cursorDimension;
        canvas.height = cursorDimension;
        context = canvas.getContext( "2d" );
        context.strokeStyle = '#fff';
        // close
        context.beginPath();
        context.moveTo(0, 0);
        context.lineTo(cursorDimension, cursorDimension);
        context.moveTo(cursorDimension, 0);
        context.lineTo(0, cursorDimension);
        context.stroke();
        cursor.close = canvas.toDataURL( 'image/png' );
        // previous
        context.clearRect(0 , 0 , canvas.width, canvas.height);
        context.beginPath();
        context.moveTo(cursorDimension, 0);
        context.lineTo(cursorDimension / 2, cursorDimension / 2);
        context.lineTo(cursorDimension, cursorDimension);
        context.stroke();
        cursor.prev = canvas.toDataURL( 'image/png' );
        // next
        context.clearRect(0 , 0 , canvas.width, canvas.height);
        context.beginPath();
        context.moveTo(0, 0);
        context.lineTo(cursorDimension / 2, cursorDimension / 2);
        context.lineTo(0, cursorDimension);
        context.stroke();
        cursor.next = canvas.toDataURL( 'image/png' );
        // zoom
        //context.clearRect(0 , 0 , canvas.width, canvas.height);
        //canvas.width = 36;
        //canvas.height = 36;
        //context.font = "36px FontAwesome";
        //context.fillText("\uf00e", 0, 0);
        //cursor.zoom = canvas.toDataURL( 'image/png' );

        function verticalAlignImage() {
            if ( !lightbox || !lightbox.isVisible() ) return;

            var top = ( window.getComputedStyle( lightbox )['height'].slice( 0, -2 )
                - image.height ) / 2 - 23;

            navigation.style.width = image.width + 'px';
            navigation.style.height = image.height + 'px';
            navigation.style.marginBottom = -image.height + 'px';
            navigation.style.top = top <= 0 ? '' : top + 'px';
            image.style.top = top <= 0 ? '' : top + 'px';
        }

        window.addEventListener( 'resize', verticalAlignImage );

        // get all images
        var images = document.getElementsByTagName( 'img'),
            link, url;

        // loop through images
        for ( var i = 0; i < images.length; i++ ) {
            // get images parent
            link = images[i].parentNode;

            // check if lightbox is needed (image parent is link width image source)
            if ( link.tagName != 'A' || !link.getAttribute( 'href').match( new RegExp( '.*(jpe?g|gif|png)$' ) ) )
                continue;

            // attach events to image (link)
            //link.addEventListener( 'mouseover', function() {
            //    this.style.cursor = 'url(' + cursor.zoom + ') 18 18, auto';
            //} );

            link.addEventListener( 'click', function( e ) {
                e.preventDefault();

                url = this.getAttribute( 'href' );

                lightbox = document.getElementById( 'lightbox' );
                if ( !lightbox ) {
                    // lightbox
                    lightbox = document.createElement( 'DIV' );
                    lightbox.id = 'lightbox';
                    document.body.appendChild( lightbox );

                    // lightbox navigation
                    navigation = document.createElement( 'DIV' );
                    navigation.id = 'lightbox__nav';
                    navigation.innerHTML = '<span class="nav-prev"></span><span class="nav-next"></span>';
                    lightbox.appendChild( navigation );

                    /**
                     * @returns {boolean}
                     */
                    lightbox.isVisible = function() {
                        return lightbox.classList.contains( 'active' );
                    };

                    /**
                     * @returns {boolean}
                     */
                    lightbox.isGallery = function() {
                        return gallery.length > 1 ? true : false;
                    };

                    /**
                     * @param url
                     * @returns {lightbox}
                     */
                    lightbox.show = function( url ) {
                        if ( typeof url == 'number' ) {
                            if ( !gallery || !gallery[url] ) return this;
                            url = gallery[url].href;
                        }

                        if ( !url ) return this;

                        lightbox.classList.add( 'loading' );

                        if ( !lightbox.isVisible() ) {
                            this.classList.add( 'active' );
                            document.body.style.overflow = 'hidden';
                            document.body.style.cursor = 'url(' + cursor.close + ') ' + cursorDimension / 2 + ' ' + cursorDimension / 2 + ' , auto';
                        }

                        // load image
                        image.onload = function() {
                            if ( !lightbox.isVisible() ) return;

                            image = this;
                            lightbox.appendChild( image );

                            lightbox.classList.remove( 'loading' );

                            verticalAlignImage();
                        };
                        image.src = url;

                        return this;
                    };

                    /**
                     * @param direction
                     * @returns {lightbox}
                     */
                    lightbox.navigate = function( direction ) {
                        if ( !lightbox.isGallery() || !direction ) return this;

                        if ( direction == 'prev' ) {
                            if (--current < 0) current = gallery.length - 1;
                        }
                        else {
                            if ( ++current > gallery.length -1 ) current = 0;
                        }

                        return lightbox.show( current );
                    };

                    /**
                     *
                     * @returns {lightbox}
                     */
                    lightbox.prev = function() {
                        return lightbox.navigate( 'prev' );
                    };

                    /**
                     * @returns {lightbox}
                     */
                    lightbox.next = function() {
                        return lightbox.navigate( 'next' );
                    };

                    /**
                     * Hide lightbox and reset variables.
                     *
                     * @returns {lightbox}
                     */
                    lightbox.hide = function() {
                        this.classList.remove( 'active' );
                        image.remove();
                        document.body.style.overflow = '';
                        document.body.style.cursor = '';
                        prev.style.display = '';
                        next.style.display = '';
                        gallery = [];

                        return this;
                    };

                    // ...
                    prev = navigation.getElementsByClassName( 'nav-prev' )[0];
                    prev.style.cursor = 'url(' + cursor.prev + ') ' + cursorDimension / 2 + ' ' + cursorDimension / 2 + ' , auto';
                    prev.addEventListener( 'click', function( e ) {
                        e.stopPropagation();
                        lightbox.prev();
                    } );

                    // ...
                    next = navigation.getElementsByClassName( 'nav-next' )[0];
                    next.style.cursor = 'url(' + cursor.next + ') ' + cursorDimension / 2 + ' ' + cursorDimension / 2 + ' , auto';
                    next.addEventListener( 'click', function( e ) {
                        e.stopPropagation();
                        lightbox.next();
                    } );

                    // ...
                    lightbox.addEventListener( 'click', function() {
                        this.hide();
                    } );
                }

                // ...
                if ( !gallery.length ) {
                    // is gallery!?
                    if ( this.parentNode.classList.contains( 'gallery-icon' ) ) {
                        var galleryItems = this.parentNode.parentNode.parentNode.querySelectorAll( 'div.gallery-icon a' );
                        for (var i = 0; i < galleryItems.length; i++) {
                            gallery[i] = galleryItems[i];
                        }
                    }
                    // no ... just one item
                    else gallery[0] = this;

                    current = gallery.indexOf( this );
                }

                // show navigation
                if ( lightbox.isGallery() ) {
                    prev.style.display = 'block';
                    next.style.display = 'block';
                }

                lightbox.show( current );
            } );
        }

        if ( lightbox ) window.lightbox = lightbox;

    } );

})( this, this.document );
