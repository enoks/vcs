/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 */
( function( window, document, undefined ) {
	var body = document.getElementsByTagName( 'body' )[0],
        header = document.getElementById( 'masthead'),
        logo = header.getElementsByClassName( 'site-branding' )[0],
        headerHeight = logo.clientHeight + 20,
        navigation = document.getElementById( 'site-navigation'),
        toggle = header.getElementsByTagName( 'button' )[0];

    // dynamic CSS
    header.style.paddingLeft = logo.clientWidth + navigation.clientWidth + 30 + 'px';
    header.style.height = headerHeight + 'px';
    //document.getElementById( 'content').style.paddingTop = headerHeight + 'px';
    navigation.style.left = logo.clientWidth + 20 + 'px';

    /**
     * Enable/disable toggle ability.
     *
     * @param toggleable
     * @returns {navigation}
     */
    navigation.toggleable = function( toggleable ) {
        if ( toggleable === undefined ) return this;
        else if ( toggleable ) {
            if ( body.className.indexOf( 'sidebar-menu__toggleable' ) < 0 ) {
                body.className += ' sidebar-menu__toggleable';

                toggle.setAttribute( 'aria-expanded', 'false' );
                navigation.setAttribute( 'aria-expanded', 'false' );
            }
        }
        else {
            body.className = body.className.replace( ' sidebar-menu__toggleable', '' );
            navigation.close();
        }

        return this;
    };

    /**
     * Toggle sidebar menu.
     *
     * @returns {navigation}
     */
    navigation.toggle = function() {
        if ( body.className.indexOf( 'menu__expanded' ) > -1 ) navigation.close();
        else navigation.expand();

        return this;
    };

    /**
     * Expand sidebar menu.
     *
     * @returns {navigation}
     */
    navigation.expand = function() {
        body.classList.add( 'menu__expanded' );
        toggle.setAttribute( 'aria-expanded', 'true' );
        navigation.setAttribute( 'aria-expanded', 'true' );

        return this;
    };

    /**
     * Close sidebar menu.
     *
     * @returns {navigation}
     */
    navigation.close = function() {
        body.classList.remove( 'menu__expanded' );
        toggle.setAttribute( 'aria-expanded', 'false' );
        navigation.setAttribute( 'aria-expanded', 'false' );

        return this;
    };

    /**
     * ...
     */
    navigation.mobile = function() {
        if ( body.offsetWidth <= 862 ) body.classList.add( 'top-menu' );
        else body.classList.remove( 'top-menu' );
    };

    window.addEventListener( 'scroll', function() {
        // sidebar is off viewport
        if ( this.pageYOffset > headerHeight + 50 ) {
            if ( body.className.indexOf( 'sidebar-menu' ) < 0 ) {
                body.className += ' sidebar-menu';
            }
        }
        else {
            body.className = body.className.replace(' sidebar-menu', '');
            navigation.toggleable( false ).close();
            //toggle.removeAttribute( 'aria-expanded' );
            //navigation.removeAttribute( 'aria-expanded' );
        }

        // 'enable' toggle
        if ( this.pageYOffset > headerHeight + 100 ) navigation.toggleable( true );
        else navigation.toggleable( false ).close();
    } );

    navigation.mobile();
    window.addEventListener( 'resize', navigation.mobile );

    toggle.addEventListener( 'click', function( e ) {
        e.stopPropagation();
        navigation.toggle();
	} );

} )( window, window.document );
