( function( $, window, document ) {

    $( document).on( 'ready', function() {

        var $user_picture_wrap = $( 'tr.user-picture-wrap' ),
            $input = $user_picture_wrap.find( 'input[name="user-picture"]' ),
            $avatar = $user_picture_wrap.find( 'img.avatar' );

        if ( !$avatar.length ) {
            $user_picture_wrap.remove();
            return;
        }

        $( '#set-user-picture' ).on( 'click', function(e) {
            e. preventDefault();

            // unset
            if ($input.val() != '') {
                $input.val('');
                $avatar.prop('src', $avatar.data('avatar'));
                return;
            }

            // set
            var $link = $(this);

            tb_show( $link.text(), $link.prop('href'), false );
        } );

        window.send_to_editor = function( $img ) {
            $img = $( $img );
            if ( !$img.is( 'img' ) ) $img = $img.find( 'img' );

            $input.val($img.attr('class').match(new RegExp('wp-image-(\\d+)'))[1]);
            $avatar.prop( 'src', $img.prop( 'src').replace(new RegExp('-\\d+x\\d+(\\.[jpe?g|png|gif]+)$'), "-150x150$1"));

            tb_remove();
        }

    } );

} )( jQuery, window, window.document );
