<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package siehste
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" type="image/png" href="<?php bloginfo( 'stylesheet_directory' ); ?>/images/favicon.png" />
<!--<link rel="shortcut icon" type="image/x-icon" href="--><?php //bloginfo( 'stylesheet_directory' ); ?><!--/images/favicon.ico" />-->
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'siehste' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php if ( $description = get_bloginfo( 'description', 'display' ) ) : ?>
			<h2 class="site-description"><?php echo $description; ?></h2>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="menu" aria-expanded="false"><?php _e( 'Primary Menu', 'siehste' ); ?></button>
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</nav><!-- #site-navigation -->

        <?php the_widget( 'Posts_Widget', array(
            'title' => 'Nächste&nbsp;Termine',
            'selected_cats' => array( 3 ),
//            'posts_per_page' => 3,
            'post_status' => 'future',
        ) ); ?>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
