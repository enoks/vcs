<?php

if ( !isset( $_GET['import'] ) ) return;

if ( $news = simplexml_load_file( dirname( __FILE__ ) . '/news.xml' ) ) {
    $posts = array();

    foreach ( $news->database->table as $data ) {
        $data = (array)$data;
        $data = (array)$data['column'];
        $data = array_map( function( $item ) {
            return (string)$item;
        }, $data );

        // post date (unfortunately no time)
        $date = strtotime( $data[1] );

        // @see http://codex.wordpress.org/Function_Reference/wp_insert_post#Parameters
        $postarr = array(
//            'post_type' => 'post',
            'post_title' => $data[2],
            'post_content' => implode( '<!--more-->', array_filter( array(
                trim( $data[3] ),
                trim( $data[4] ),
            ) ) ),
            'post_status' => 'publish',
            'post_date' => date( 'Y-m-d H:i:s', $date ),
            'post_date_gmt' => date( 'Y-m-d H:i:s', (int)current_time( $date, 1 ) ),
            'post_author' => 3,
            'post_category' => array(
                'news' => 1,
                'turnier' => 2,
                'termin' => 3,
                'veranstaltung' => 4,
                'pokal' => 1,
            ),
        );

        // ...
        if ( !array_key_exists( $data[6], $postarr['post_category'] ) ) continue;

        // define category
        $postarr['post_category'] = array( $postarr['post_category'][$data[6]] );

        if ( !isset( $posts[$date] ) ) $posts[$date] = array();
        $posts[$date][] = array_filter( $postarr );
    }

    // sort by timestamp
    ksort( $posts );

    // create post
    foreach ( $posts as $dayposts ) {
        foreach ( $dayposts as $post ) {
            if ( ( $post_id = wp_insert_post( $post ) ) && in_array( 2, $post['post_category'] ) ) {
                // turnier = gallery
//                set_post_format( $post_id, 'gallery' );
            }
        }
    };
}
$end = true;
