<?php

/**
 * ...
 */
add_shortcode( 'permalink', 'permalink_shortcode' );
function permalink_shortcode( $atts, $content = '' ) {
    extract( shortcode_atts( array(
        'post' => null,
        'category' => null,
        'text' => '',
    ), $atts) );

	if ( !$text ) $text = $content;

    // post
    if ( $post && ( $link = get_permalink( $post ) ) ) {
        if ( !$text ) $text = get_the_title( $post );
    }
    // category
    elseif ( $category && ( $link = get_category_link( $category ) ) ) {
        if ( !$text ) $text = get_cat_name( $category );
    }
    else return;

    return '<a href="' . $link . '">' . $text . '</a>';
}
