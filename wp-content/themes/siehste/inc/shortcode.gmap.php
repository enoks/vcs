<?php

/**
 * ...
 */
add_shortcode( 'gmap', 'gmap_shortcode' );
function gmap_shortcode( $atts, $content = '' ) {
	if ( !is_array( $atts ) ) $atts = array();

	$atts += array(
		'dimension' => '16:9',
		'address' => '',
	);

	if ( empty( $atts['address'] ) ) {
		$atts['address'] = $content;
	};

	// calculate dimension
	if ( preg_match( '/((\d+)(px|%)?)((:|x)(\d+)(px|%)?)?/', $atts['dimension'], $dimension ) ) {
		$width = $dimension[2];
		$wUnit = $dimension[3];
		$height = $dimension[6];
		$hUnit = isset($dimension[7]) ? $dimension[7] : '';

		// square
		if ( !$dimension[5] || !$height ) {
			$height = '100';
			$hUnit = '%';
		}
		// ratio
		elseif ( $dimension[5] == ':' ) {
			$height = $height * 100 / $width;
			$hUnit = '%';

			unset( $width, $wUnit );
		}
		// 'fixed' dimension
		else {
			if ( $hUnit != '%' ) {
				$height = $height * 100 / $width;
				$hUnit = '%';
			}
		}

		if ( !empty( $width ) ) {
			$width .= ( $wUnit ? $wUnit : 'px' );
			$width = " style=\"width:$width;\"";
		}

		if ( !empty( $height ) ) {
			$height .= $hUnit;
			if ( $hUnit == '%' ) $height = " padding-top:$height;";
			else $height = " height:$height;";
		}
	}

	return '<div class="gm-wrapper loading"' . ( !empty( $width ) ? ' style="' . $width . '"' : '' ) . '>' .
	       '<div class="gm-canvas"' . ( !empty( $height ) ? ' style="' . $height . '"' : '' ) . '>' .
	       '<address>' . $atts['address'] . '</address>' .
	       '<div class="info">' . $content . '</div></div></div>';
}
