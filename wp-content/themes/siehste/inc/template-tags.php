<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package siehste
 */

function siehste_posts_navigation( $args = array() ) {
    echo '<div class="nav-links"><div>'
        . paginate_links()
        . '</div></div>';
}

if ( ! function_exists( 'siehste_posted_on' ) ) :
/**
 * Prints HTML with meta information for the current post-date/time and author.
 */
function siehste_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( sprintf( __( "Last updated %s o'clock", 'siehste' ), get_the_modified_date( get_option( 'date_format' ) . ' - H:i ' ) ) )
	);

    $posted_on = sprintf(
        _x( '%s', 'post date', 'siehste' ),
        '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
    );
    echo '<span class="posted-on">' . $posted_on . '</span>';

    if ( 'post' == get_post_type() ) {
        /* translators: used between list items, there is a space after the comma */
        $categories_list = get_the_category_list( __( ', ', 'siehste' ) );
        if ( $categories_list && siehste_categorized_blog() ) {
            printf( '<span class="cat-links">' . __( '%1$s', 'siehste' ) . '</span>', $categories_list );
        }
    }

    if ( $tagList = get_the_tag_list() ) {
        echo '<span class="tag-links">' .  $tagList . '</span>';
    }

}
endif;

/**
 * Prints HTML with meta information for the categories, tags and comments.
 */
function siehste_entry_footer() {
    return '';
}

/**
 * ...
 */
function siehste_post_thumbnail( $size = 'post-thumbnail', $echo = true ) {
	if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) return;

	// get image meta
	$image_meta = get_post_meta( get_post_thumbnail_id(), '_wp_attachment_metadata', TRUE );
	// check orientation
	$orientation = 'square';
	if ( $image_meta['height'] > $image_meta['width'] ) $orientation = 'portrait';
	elseif ( $image_meta['height'] < $image_meta['width'] ) $orientation = 'landscape';

	$html = '';

	if ( is_singular() ) $html .= '<div class="post-thumbnail ' . $orientation . '">';
	else $html .= '<a class="post-thumbnail ' . $orientation . '" href="' . get_the_permalink() . '" aria-hidden="true">';

	$html .= get_the_post_thumbnail( null, $size, array( 'alt' => get_the_title(), 'title' => '' ) );

	if ( is_singular() ) $html .= '</div>';
	else $html .= '</a>';

	if ( !$echo ) return $html;

	echo $html;
}

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function siehste_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'siehste_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'siehste_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so siehste_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so siehste_categorized_blog should return false.
		return false;
	}
}

/**
 * Flush out the transients used in siehste_categorized_blog.
 */
function siehste_category_transient_flusher() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'siehste_categories' );
}
add_action( 'edit_category', 'siehste_category_transient_flusher' );
add_action( 'save_post',     'siehste_category_transient_flusher' );
