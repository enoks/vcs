<?php

/**
 * Change image src to data-src for lazy loading images.
 */
add_filter( 'wp_get_attachment_image_attributes', 'siehste_attachment_image_attributes' );
function siehste_attachment_image_attributes( $attr ) {
	if ( !is_admin() ) {
		$attr['data-src'] = $attr['src'];
		$attr['src'] = get_template_directory_uri() . '/images/blank.gif';

		// since WP 4.4 srcset is in use
		// that interferes with our 'image as background' functionality
		// Todo: somehow bring them in harmony
		unset( $attr['srcset'], $attr['sizes'] );
	}

    return $attr;
}

/**
 * Add extra fields to media uploader.
 *
 * @param $form_fields
 * @param $post
 * @return mixed
 */
add_filter( 'attachment_fields_to_edit', 'siehste_attachment_fields_to_edit', 10, 2 );
function siehste_attachment_fields_to_edit( $form_fields, $post ) {
    $form_fields['siehste-attachment-link'] = array(
        'label' => 'Link',
//        'input' => 'text',
        'value' => get_post_meta( $post->ID, '_siehste_attachment_link', true ),
//        'helps' => '',
    );

    return $form_fields;
}

/**
 * Save values extra fields in media uploader.
 *
 * @param $post
 * @param $attachment
 * @return mixed
 */
add_filter( 'attachment_fields_to_save', 'siehste_attachment_fields_to_save', 10, 2 );
function siehste_attachment_fields_to_save( $post, $attachment ) {
    if( isset( $attachment['siehste-attachment-link'] ) )
        update_post_meta( $post['ID'], '_siehste_attachment_link', esc_url( $attachment['siehste-attachment-link'] ) );

    return $post;
}

/**
 * Delete extra attachment fields on attachment deletion.
 *
 * @param $postid
 */
add_filter( 'delete_attachment', 'siehste_delete_attachment', 10, 2 );
function siehste_delete_attachment( $postid ) {
    delete_post_meta( $postid, '_siehste_attachment_link' );
}

/**
 * Link images (link="file") to large attachment instead of original.
 */
add_filter( 'wp_get_attachment_link', 'siehste_get_attachment_link', 10, 4 );
function siehste_get_attachment_link( $content, $post_id, $size, $permalink ) {
    // Only do this if we're getting the file URL
    if ( !$permalink ) {
        // This returns an array of (url, width, height)
        $image = wp_get_attachment_image_src( $post_id, 'large' );
        $content = preg_replace( '/href=\'(.*?)\'/', 'href=\'' . $image[0] . '\'', $content );
    }

    return $content;
}
