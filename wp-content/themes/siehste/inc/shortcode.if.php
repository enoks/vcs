<?php

/**
 * ...
 */
add_shortcode( 'if', 'if_shortcode' );
function if_shortcode( $atts, $content = '' ) {
	$atts = shortcode_atts( array(
		'callback' => '',
	), $atts );

	if ( $content && $atts['callback'] && function_exists( $atts['callback'] ) ) {
		if ($atts['callback']()) {
			return $content;
		}
	}
}
