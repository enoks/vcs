<?php

/**
 * ...
 */
class Posts_Widget extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'widget_posts',
            'description' => __( 'Your site&#8217;s posts.'),
        );
        parent::__construct( 'posts', __( 'Posts' ), $widget_ops );
    }

    public function widget( $args, $instance ) {

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        echo $args['before_widget'];
        if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

        if ( $posts = get_posts( array(
            'categories' => implode( ',', $instance['selected_cats'] ? $instance['selected_cats'] : array() ),
            'posts_per_page' => isset($instance['posts_per_page']) ? $instance['posts_per_page'] : -1,
            'post_status' => $instance['post_status'],
        ) ) ) {
            foreach ( $posts as &$post ) {
                $post = '<li class="post"><a href="' . get_permalink( $post->ID ) . '" title="' . $post->post_title . '">'
                    . '<span class="post__day">' . get_the_date( 'D', $post->ID ) . '</span>'
                    . '<span class="post__date">' . get_the_date( '', $post->ID ) . '</span>'
                    . '<span class="post__title">' . $post->post_title . '</span>'
                    . '</a></li>';
            }

            echo '<ul class="posts" data-wp-lists="list:events">' . implode( "\n", array_reverse( $posts ) ) . '</ul>';
        }
        else echo '<p>In näherer Zukunft ist nix geplant.</p>';

        echo $args['after_widget'];
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['post_status'] = $new_instance['post_status'];
        $instance['selected_cats'] = array_filter(array_unique($new_instance['selected_cats']));

        return $instance;
    }

    public function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
        $title = strip_tags($instance['title']);
        $post_status = $instance['post_status'] ? $instance['post_status'] : 'publish';
        $selected_cats = $instance['selected_cats'] ? $instance['selected_cats'] : array();
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

        <p><label for="<?php echo $this->get_field_id('post_status'); ?>"><?php _e('Status:'); ?></label><br />
            <select id="<?php echo $this->get_field_id('post_status'); ?>" name="<?php echo $this->get_field_name('post_status'); ?>">
                <option<?php echo ( $post_status == 'publish' ? ' selected' : '' ); ?> value="publish"><?php _e('Published'); ?></option>
                <option<?php echo ( $post_status == 'future' ? ' selected' : '' ); ?> value="future"><?php _e('Future'); ?></option>
            </select></p>

        <?php if ( $categories = get_categories() ) : ?>
        <label for="<?php echo $this->get_field_id('selected_cats'); ?>"><?php _e('Categories:'); ?></label>
        <ul data-wp-lists="list:category" class="categorychecklist form-no-clear">
            <?php foreach ( get_categories() as $category ): ?>
            <li><label class="selectit">
                <input<?php echo in_array( $category->cat_ID, $selected_cats ) ? ' checked="checked"' : ''; ?> value="<?php echo $category->cat_ID ?>" type="checkbox" name="<?php echo $this->get_field_name('selected_cats'); ?>[]">
                <?php echo $category->cat_name; ?>
            </label></li>
            <?php endforeach; ?>
        </ul>
        <?php endif;
    }
}

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'posts__enqueue_scripts' );
function posts__enqueue_scripts() {
    wp_enqueue_style( 'posts', get_template_directory_uri() . '/inc/widgets/posts/posts.css' );
}
