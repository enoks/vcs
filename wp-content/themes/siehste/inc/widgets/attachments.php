<?php

/**
 * ...
 */
class Attachments_Widget extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'widget_attachments',
            'description' => __( 'Post&#8217;s attachments.'),
        );
        parent::__construct( 'attachments', __( 'Attachments' ), $widget_ops );
    }

    public function widget( $args, $instance ) {

        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

        echo $args['before_widget'];
        if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }

	    $postids = array();

	    // The Query
	    $the_query = new WP_Query( array(
		    'posts_per_page' => -1,
	    ) + $instance['args'] );

		// The Loop
	    if ( $the_query->have_posts() ) {
		    while ( $the_query->have_posts() ) {
			    $the_query->the_post();

			    $postids[] = get_the_ID();
		    }
	    }

	    /* Restore original Post Data */
	    wp_reset_postdata();

	    foreach ( get_posts( $vidArgs = array(
		    'post_parent__in' => $postids,
		    'post_type' => 'attachment',
		    'post_mime_type'=>'image',
		    'posts_per_page'=> ( !empty( $instance['size'] ) ? $instance['size'][0] * $instance['size'][1] : 9 ),
		    'orderby' => 'rand',
	    ) ) as $attachment ) {
		    if ( $attachment->post_parent == get_the_ID() ) continue;
		    $attachments[] = $attachment;
	    }

	    $gallery = do_shortcode( '[gallery ids="' . implode( ',', array_map( function( $item ) {
			    return $item->ID;
		    }, $attachments ) ) . '" columns="' . ( !empty( $instance['size'][0] ) ? $instance['size'][0] : 3 ) . '"  size="large" link="file"]' );

	    $nb = 0;
	    $gallery = preg_replace_callback( "/href='[^']+'/", function() use ( $attachments, &$nb ) {
		    return 'href="' . get_permalink( $attachments[$nb]->post_parent ) . '" title="' . get_the_title( $attachments[$nb++]->post_parent ) .'"';
	    }, $gallery );

	    echo $gallery;
        echo $args['after_widget'];
    }

    public function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags( $new_instance['title'] );

        $instance['args'] = array();
	    foreach ( explode( '&', $new_instance['args'] ) as $arg ) {
		    $arg = explode( '=', $arg );
		    if ( count( $arg ) != 2 ) continue;

		    $instance['args'][$arg[0]] = $arg[1];
	    }

        $instance['size'] = explode( ':', $new_instance['size'] );
	    for ( $i = 0; $i < 2; $i++ ) {
		    if ( empty( $instance['size'][$i] ) || !is_numeric( $instance['size'][$i] ) )
			    $instance['size'][$i] = ( !$i ? 3 : 2 );

		    $instance['size'][$i] = abs( (int) $instance['size'][$i] );
	    }

        return $instance;
    }

    public function form( $instance ) {
        $instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
        $title = $instance['title'];

	    $args = ( $instance['args'] ? $instance['args'] : array() );
	    foreach ( $args as $key => &$value ) {
		    $value = $key . '=' . $value;
	    }
	    $args = implode( '&', $args );

        $size = ( !empty( $instance['size'] ) ? implode( ':', $instance['size'] ) : '3:2' ); ?>

        <p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title' ); ?>:</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'args' ); ?>"><?php _e( 'Post filter' ); ?>:</label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'args' ); ?>" name="<?php echo $this->get_field_name( 'args' ); ?>" type="text" value="<?php echo $args; ?>" /></p>

        <p><label for="<?php echo $this->get_field_id( 'size' ); ?>"><?php _e( 'Columns:Rows' ); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id( 'size' ); ?>" placeholder="3:2" name="<?php echo $this->get_field_name( 'size' ); ?>" type="text" value="<?php echo $size; ?>" /></p>

        <?php
    }
}
