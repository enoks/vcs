<?php

define( 'SIEHSTE_WIDGETS_DIR', untrailingslashit( dirname( __FILE__ ) ) );

require_once SIEHSTE_WIDGETS_DIR . '/posts/posts.php';
require_once SIEHSTE_WIDGETS_DIR . '/attachments.php';

/**
 * Registers siehste widget.
 */
add_action( 'widgets_init', 'siehste_widgets_init' );
function siehste_widgets_init() {
    register_widget( 'Posts_Widget' );
    register_widget( 'Attachments_Widget' );
}
