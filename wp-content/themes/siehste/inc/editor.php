<?php

// define available editor buttons
add_filter( 'mce_buttons', 'siehste__mce_buttons', 20, 1 );
function siehste__mce_buttons( $buttons ) {
    return array( 'formatselect', 'bold', 'italic', 'backcolor', 'link', 'unlink', 'bullist', 'numlist', 'undo', 'redo', 'wp_more', 'fullscreen' );
}

// remove second row buttons
add_filter( 'mce_buttons_2', 'siehste__mce_buttons_2' );
function siehste__mce_buttons_2( $buttons ) {
    return array();
}

// only allow certain block formats
add_filter( 'tiny_mce_before_init', 'siehste__tiny_mce_before_init' );
function siehste__tiny_mce_before_init( $init ) {
    // block formats
    $init['block_formats'] = implode( ';', array(
        __( 'Paragraph' ) . '=p',
        __( 'Subheading', 'siehste' ) . '=h2',
    ) );

	// remove all formats for pasted text
	$init['paste_as_text'] = true;

	// colors
	$init['textcolor_map'] = '["f6cfd3", "rot", "cee8d7", "grün", "d6e0eb", "blau"]';

    return $init;
}

// ...
add_action( 'admin_head', 'siehste__admin_head' );
function siehste__admin_head() {
    add_editor_style( get_template_directory_uri() . '/css/editor.css?' . time() );
}
