<?php

/**
 * Enqueue scripts and styles.
 */
add_action( 'admin_enqueue_scripts', 'siehste_user_picture__admin_enqueue_scripts', 10, 1 );
function siehste_user_picture__admin_enqueue_scripts( $hook ) {
	if ( ! in_array( $hook, array( 'user-new.php', 'user-edit.php', 'profile.php' ) ) ) {
		return;
	}

	wp_enqueue_script( 'jquery' );
	add_thickbox();
	wp_enqueue_script( 'media-upload' );

	wp_enqueue_script( 'user-picture', get_template_directory_uri() . '/js/user.picture.admin.js' );
}

/**
 * ...
 */
add_action( 'personal_options', 'siehste_user_picture__personal_options', 0, 1 );
function siehste_user_picture__personal_options( $profileuser ) {
	if ( ! $avatar = get_avatar( $profileuser->ID ) ) {
		return;
	}

	$attachment = get_user_attachment( $profileuser->ID ); ?>
	<tr class="user-picture-wrap">
		<th><label for="picture"><?php echo __( 'User picture', 'siehste' ) ?></label></th>
		<td>
			<input type="hidden" name="user-picture" value="<?php echo $attachment->ID; ?>"/>
			<?php echo $avatar; ?>&nbsp;

			<a id="set-user-picture" class="button"
			   href="<?php echo get_option( 'siteurl' ) ?>/wp-admin/media-upload.php?type=image&referer=set-user-picture&TB_iframe=1&post_id=0">
				<?php echo __( 'Set or unset user picture', 'siehste' ) ?>
			</a>
		</td>
	</tr>
<?php }

add_action( 'personal_options_update', 'siehste_user_picture__personal_options_update' );
add_action( 'edit_user_profile_update', 'siehste_user_picture__personal_options_update' );
function siehste_user_picture__personal_options_update( $user_id ) {
	if ( empty( $_POST['user-picture'] ) ) {
		delete_user_meta( $user_id, 'picture' );
	} else {
		update_user_meta( $user_id, 'picture', sanitize_text_field( $_POST['user-picture'] ) );
	}
}

/**
 * ...
 */
function get_user_attachment( $user_id ) {
	return get_post( get_user_meta( $user_id, 'picture', TRUE ) );
}

/**
 * ...
 */
add_filter( 'pre_get_avatar', 'siehste_user_picture__pre_get_avatar', 0, 3 );
function siehste_user_picture__pre_get_avatar( $null, $id_or_email, $args ) {
	if ( $picture = get_user_attachment( $id_or_email ) ) {
		$picture = wp_get_attachment_image_src( $picture->ID );

		$avatar = get_avatar_data( $id_or_email, $args );
		$avatar = $avatar['url'];

		$picture = sprintf(
			"<img alt='%s' src='%s' data-avatar='%s' class='%s' height='%d' width='%d' %s/>",
			esc_attr( $args['alt'] ),
			esc_url( $picture[0] ? $picture[0] : $avatar ),
			esc_url( $avatar ),
			esc_attr( join( ' ', array( 'avatar', 'avatar-' . (int) $args['size'], 'photo' ) ) ),
			(int) $args['height'],
			(int) $args['width'],
			$args['extra_attr']
		);
	}

	return $picture ? $picture : NULL;
}

/**
 * ...
 */
add_action( 'admin_init', 'siehste_user_picture__admin_init' );
function siehste_user_picture__admin_init() {
	global $pagenow;

	if ( in_array( $pagenow, array( 'media-upload.php', 'async-upload.php' ) ) ) {
		add_filter( 'gettext', function ( $translated_text, $text, $domain ) {
			if ( $text == "Insert into Post" ) {
				if ( strpos( wp_get_referer(), 'set-user-picture' ) !== FALSE ) {
					return __( 'Set this as user picture', 'siehste' );
				}
			}

			return $translated_text;
		}, 1, 3 );
	}
}
