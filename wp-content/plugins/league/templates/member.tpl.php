<article class="member member-<?php echo $member->ID; ?><?php echo( !empty( $in_squad ) ? ' checked' : '' ) ?>">

	<header class="entry-header">
		<h1 class="entry-title">
			<?php $nickname = get_user_meta( $member->ID, 'nickname', TRUE );
			if ( $nickname && strpos( $member->data->display_name, $nickname ) === false ) {
				printf( __( '<span>%s </span><span>aka "%s"</span>', 'league' ), $member->data->display_name, $nickname );
			} else {
				echo '<span>' . $member->data->display_name . '</span>';
			} ?>
		</h1>
	</header>
	<!-- .entry-header -->

	<div class="entry-content">
		<?php echo get_avatar( $member->ID, 150 );

		$description = get_user_meta( $member->ID, 'description', TRUE );
		echo '<ul class="member__info">';
		if ( $description = array_filter( explode( "\n", $description ) ) ) {
			foreach ( $description as $data ) {
				if (!trim($data)) continue;

				list( $key, $value ) = explode( ':', $data );
				echo '<li><span class="label">' . trim($key) . '</span><span>' . trim($value) . '</span></li>';
			}
		}
		else echo '<li></li>';
		echo '</ul>'; ?>

	</div>
	<!-- .entry-content -->
</article><!-- #member-## -->