<?php

/**
 * Plugin Name: League
 * Description: League Manager
 * Version: 1.0.0
 * Author: Stefan Käsche
 */

if ( ! defined( 'BVV_API_KEY' ) ) define( 'BVV_API_KEY', 'e1997ee0-6d46-418a-8eb0-9f51a10ceb11' );
if ( ! defined( 'BVV_CLUB_ID' ) ) define( 'BVV_CLUB_ID', 15790 );

// LLSF = 534445
// LKSM = 534242

/**
 * On plugin deactivation.
 */
register_deactivation_hook( __FILE__, 'league_deactivation' );
function league_deactivation() {
  global $wpdb;

  // delete all posts
  foreach ( array( 'season', 'team' ) as $post_type ) {
    $sql = "DELETE FROM $wpdb->posts WHERE post_type = '$post_type'";
    $wpdb->query($sql);
  }

  // delete orphan relationships (not only seasons)
  $wcu_sql = "DELETE FROM $wpdb->term_relationships WHERE term_taxonomy_id=1 AND object_id NOT IN (SELECT id FROM $wpdb->posts)";
  $wpdb->query($wcu_sql);
  // delete orphan post meta (not only seasons)
  $wcu_sql = "DELETE pm FROM $wpdb->postmeta pm LEFT JOIN $wpdb->posts wp ON wp.ID = pm.post_id WHERE wp.ID IS NULL";
  $wpdb->query($wcu_sql);

  // delete taxonomies
  foreach ( array( 'league', 'team' ) as $taxonomy ) {
    $sql = "DELETE FROM $wpdb->term_taxonomy WHERE taxonomy = '$taxonomy'";
    $wpdb->query($sql);
  }

  // delete orphan taxonomy terms (not only seasons)
  $sql = "DELETE t FROM $wpdb->terms t LEFT JOIN $wpdb->term_taxonomy tt ON tt.term_id = t.term_id WHERE tt.term_id IS NULL";
  $wpdb->query($sql);
}

/**
 * 'Rearrange' league's admin menu.
 */
add_action( 'admin_menu', 'league__admin_menu' );
function league__admin_menu() {
  global $menu, $submenu;

  foreach ( $menu as $weight => $item ) {
    // search for Seasons top level menu item
    if ( $menu[$weight][0] != __( 'Seasons', 'league' ) ) continue;

    // change item name
    $menu[$weight][0] = __( 'League Manager', 'league' );
    break;
  }

  // add team menu to league's one
  if ( !empty( $submenu['edit.php?post_type=team'] ) ) {
    foreach ( $submenu['edit.php?post_type=team'] as $item ) {
      $submenu['edit.php?post_type=season'][(count($submenu['edit.php?post_type=season']) + 1) * 5] = $item;
    }
  }

  // remove from menu
  remove_submenu_page( 'edit.php?post_type=season', 'post-new.php?post_type=season' );
  remove_submenu_page( 'edit.php?post_type=season', 'post-new.php?post_type=team' );
  remove_menu_page( 'edit.php?post_type=team' );

  // rearrange submenu
  if ( !empty( $submenu['edit.php?post_type=season'] ) ) {
    foreach ( $submenu['edit.php?post_type=season'] as $key => $item ) {
      if ( $item[2] == 'edit-tags.php?taxonomy=team&amp;post_type=season' ) break;
      $key = false;
    }

    if ( $key ) {
      $submenu['edit.php?post_type=season'][] = $submenu['edit.php?post_type=season'][$key];
      unset( $submenu['edit.php?post_type=season'][$key] );
    }
  }
}

/**
 * Implements after_setup_theme action.
 */
add_action( 'after_setup_theme', 'league__after_setup_theme' );
function league__after_setup_theme() {
  load_theme_textdomain( 'league', WP_PLUGIN_DIR . '/league/languages' );
}

// load BVV xml
function bvv_get( $endpoint, $query = array(), $cache = true ) {
	if ( !extension_loaded('simplexml' ) || !ini_get('allow_url_fopen' ) ) return null;

	$query = array( 'apiKey' => BVV_API_KEY ) + $query;

	$cache_key = 'bvv_' . base_convert( md5( $endpoint . '|' . serialize( $query ) ), 10, 36 );
	if ( !$xml = get_transient( $cache_key ) ) {
		set_transient(
			$cache_key,
			($xml = simplexml_load_file(add_query_arg( $query, "https://bbvv.sams-server.de/xml/{$endpoint}.xhtml" ) ))->asXML(),
			DAY_IN_SECONDS
		);
	}
	else $xml = simplexml_load_string( $xml );

	return $xml;
}

// includes
include( WP_PLUGIN_DIR . '/league/inc/calculator.php' );

include( WP_PLUGIN_DIR . '/league/inc/season.php' );
include( WP_PLUGIN_DIR . '/league/inc/team.php' );
include( WP_PLUGIN_DIR . '/league/inc/league.php' );
include( WP_PLUGIN_DIR . '/league/inc/member.php' );
include( WP_PLUGIN_DIR . '/league/inc/venue.php' );

include( WP_PLUGIN_DIR . '/league/inc/season.shortcodes.php' );
include( WP_PLUGIN_DIR . '/league/inc/season.widgets.php' );
