<?php

add_shortcode( 'league_season_table', 'league_season_table_shortcode' );
function league_season_table_shortcode( $atts ) {
	$atts = shortcode_atts( array(
		'seasonid' => get_the_ID(), // season ID
		'title' => '',
	), $atts );

	if ( empty( $atts['seasonid'] ) ) {
		return '';
	}

	$table_classes = array( 'season-table' );

	// table
	if ( $atts['title'] ) {
		$caption = '<caption><h2>' . $atts['title'] . '</h2></caption>';
	}

	$header = '<colgroup>'
	          . '<col class="rank" width="1" />'
	          . '<col class="team" />'
	          . '<col class="matches" width="1" />'
	          . '<col class="sets" width="1" />'
	          . '<col class="score" width="1" />'
	          . '<col class="difference" width="1" />'
	          . '</colgroup>'

	          . '<thead><tr>'
	          . '<th class="rank">' . __( 'Rank', 'league' ) . '</th>'
	          . '<th class="team">' . __( 'Team', 'league' ) . '</th>'
	          . '<th class="matches">' . __( 'Matches', 'league' ) . '</th>'
	          . '<th class="sets">' . __( 'Sets', 'league' ) . '</th>'
	          . '<th class="score">' . __( 'Points', 'league' ) . '</th>'
	          . '<th class="difference">' . __( 'Difference', 'league' ) . '</th>'
	          . '</tr></thead>';

	$body = '<tbody>';

	// BVV SAMS
	if ( $bvv_msid = get_post_meta( $atts['seasonid'], '_bvv_msid', true ) ) {
		if ( $match_series = bvv_get('rankings', array( 'matchSeriesId' => $bvv_msid ) ) ) {
			if ( count( $team = get_post_meta( $atts['seasonid'], '_season_team', true ) ) > 1 ) {
				$team = null;
			} elseif ( $team = current( $team ) ) {
				$team = '<a href="' . get_permalink( $team ) . '"><strong>VCS ' . get_the_title( $team ) . '</strong></a>';
			}

			foreach ( $match_series->ranking as $ranking ) {
				$body .= '<tr' . ( $ranking->team->name == 'VC Strausberg' ? ' class="hometeam"' : '' ) . '>'
				         . '<td class="rank">' . $ranking->place . '</td>'
				         . '<td class="team">' . ( $ranking->team->name == 'VC Strausberg' ? ( $team ?: '<strong>' . $ranking->team->name . '</strong>' ) : $ranking->team->name ) . '</td>'
				         . '<td class="matches">' . $ranking->matchesPlayed . '</td>'
				         . '<td class="sets">' . $ranking->setPoints . '</td>'
				         . '<td class="score">' . $ranking->points . '</td>'
				         . '<td class="difference">' . $ranking->ballPointDifference . '</td>'
				         . '</tr>';
			}
		}
		else {
			$table_classes[] = 'no-data';
			$body .= '<tr><td colspan="6">' . __( 'Unable to load rankings from BVV.', 'league' ) . '</td></tr>';
		}
	}
	else if ( $table = get_post_meta( $atts['seasonid'], '_season_table', TRUE ) ) {
		$rank = 0;

		// loop table
		foreach ( $table as $nb => $data ) {
			if ( ! $team = league_get_team( $data['team'], 'link', get_post_meta( $atts['seasonid'], '_season_year', TRUE ) ) ) {
				$team = '<i>FEHLER :/</i>';
			}

			// increase rank
			if ( ! $nb
			     || $table[ $nb - 1 ]['score'] > $data['score']
			     || $table[ $nb - 1 ]['points'][0] - $table[ $nb - 1 ]['points'][1] > $data['points'][0] - $data['points'][1]
			     || $table[ $nb - 1 ]['sets'][0] - $table[ $nb - 1 ]['sets'][1] > $data['sets'][0] - $data['sets'][1]
				 || ( !$data['matches'] && $table[ $nb - 1 ]['matches'] )
			) {
				$rank = $nb + 1;
			}

			$body .= '<tr' . ( strpos( $team, '<a' ) === 0 ? ' class="hometeam"' : '' ) . '>'
			         . '<td class="rank">' . $rank . '</td>'
			         . '<td class="team">' . $team . '</td>'
			         . '<td class="matches">' . $data['matches'] . '</td>'
			         . '<td class="sets">' . $data['sets'][0] . ' : ' . $data['sets'][1] . '</td>'
			         . '<td class="score">' . $data['score'] . '</td>'
			         . '<td class="difference">' . ( is_numeric( $data['points'] ) ? $data['points'] : $data['points'][0] - $data['points'][1] ) . '</td>'
			         . '</tr>';
		}
	} else {
		$table_classes[] = 'no-data';
		$body .= '<tr><td colspan="6">' . __( 'No table available.', 'league' ) . '</td></tr>';
	}

	$body .= '</tbody>';

	$content = '<table class="' . implode( ' ', $table_classes ) . '">'
	           . ( ! empty( $caption ) ? $caption : '' )
	           . $header
	           . $body
	           . '</table>';

	return $content;
}

add_shortcode( 'league_season_schedule', 'league_season_schedule_shortcode' );
function league_season_schedule_shortcode( $atts = array() ) {
	$atts = shortcode_atts( array(
		'seasonid' => get_the_ID(), // season ID
		'hometeams' => FALSE,
		'title' => '',
	), $atts );

	if ( empty( $atts['seasonid'] ) ) {
		return '';
	}

	// season settings
	$settings = get_season_settings( $atts['seasonid'] );

	// schedule
	if ( $atts['title'] ) {
		$caption = '<caption><h2>' . $atts['title'] . '</h2></caption>';
	}

	$header = '<colgroup class="matchday" span="2" width="1"><col /><col /></colgroup>'
	          . '<colgroup class="match" span="3" /><col /><col /><col /></colgroup>'
	          . '<col class="score" width="1" /></col>'
	          . '<thead><tr>'
	          . '<th class="matchday" colspan="2">' . __( 'Matchday', 'league' ) . '</th>'
	          . '<th class="match" colspan="3">' . __( 'Match', 'league' ) . '</th>'
	          . '<th class="score">' . __( 'Score', 'league' ) . '</th>'
	          . '</tr></thead>';

	$body = '<tbody>';

	// BVV SAMS
	if ( $bvv_msid = $settings['bvv_msid'] ) {
		if ( count($team = $settings['teams']) > 1 ) $team = null;
		elseif ( $team = current($team) ) $team = '<a href="' . get_permalink( $team ) . '"><strong>VCS ' . get_the_title( $team ) . '</strong></a>';

		if ( $matches = bvv_get( 'matches', array( 'matchSeriesId' => $bvv_msid ) ) ) {
			foreach ( $matches->match as $match ) {
				$status = '';
				if ( in_array( 'VC Strausberg', array( $match->team[0]->name, $match->team[1]->name ) ) ) {
					if ( $sets = array_filter(explode( ':', $match->results->setPoints ), function( $sets ) { return is_numeric($sets); }) ) {
						$status = ' ok';

						if ( $match->team[0]->name == 'VC Strausberg' && $sets[0] < $sets[1] ) $status = ' error';
						elseif ( $match->team[1]->name == 'VC Strausberg' && $sets[0] > $sets[1] ) $status = ' error';
					}
				}
				// don't show matches without hometeam
				elseif ( $atts['hometeams'] ) continue;

				$body .= '<tr>';
				$body .= '<td class="matchday" colspan="2">' . $match->date . '</td>';
				$body .= '<td class="number">' . "#{$match->number}" . '</td>';
				$body .= '<td class="team">' . ($match->team[0]->name == 'VC Strausberg' ? ($team ?: '<strong>' . $match->team[0]->name . '</strong>') : $match->team[0]->name) . '</td>';
				$body .= '<td class="team">' . ($match->team[1]->name == 'VC Strausberg' ? ($team ?: '<strong>' . $match->team[0]->name . '</strong>') : $match->team[1]->name) . '</td>';

				$set_points = array();
				if ( isset($match->results->sets) ) {
					$set_points = array(array(), array());

					foreach ( $match->results->sets->set as $set ) {
						$points = explode( ':', $set->points );
						$set_points[0][] = $points[0];
						$set_points[1][] = $points[1];
					}

					$set_points = array_map( function( $points ) { return implode( '+', $points ); }, $set_points );
					$set_points = implode( ',', array_filter( $set_points ) );
				}
				$body .= '<td class="score' . $status . '"' . (!empty($set_points) ? ' data-points="' . $set_points . '"' : '') . '>' . $match->results->setPoints . '</td>';
				$body .= '</tr>';
			}
		}
		else {
			$table_classes[] = 'no-data';
			$body .= '<tr><td colspan="6">' . __( 'Unable to load matches from BVV.', 'league' ) . '</td></tr>';
		}
	}
	elseif ( $schedule = get_post_meta( $atts['seasonid'], '_season_schedule', TRUE ) ) {
		$round = 0;

		// loop all matchdays
		foreach ( $schedule as $matchday => $matches ) {
			$matchday = mysql2date( 'd.m.y', $matchday );

			// don't show matches without hometeam
			if ( $atts['hometeams'] ) {
				$pattern = 'p';
				if ( get_post_type() == 'team' ) $pattern .= get_the_ID() . '$';
				$matches = array_filter( $matches, function ( $teams ) use ( $pattern ) {
					return preg_grep( '/^' . $pattern . '/', array_keys( $teams ) );
				} );

				// reset keys
				$matches = array_values( $matches );
			}

			// loop all matches on matchday
			foreach ( $matches as $nb => $match ) {
				$body .= '<tr>';

				if ( ! $nb ) {
					$body .= '<td class="matchday" rowspan="' . count( $matches ) . '">' . ++ $round . '</td>';
				}
				$body .= '<td class="matchday">' . $matchday . '</td>';

				$body .= '<td class="number">' . ( $match['nb'] ? '#' . $match['nb'] : '' ) . '</td>';

				$sets = array();
				$points = array();
				$status = '';

				// match
				foreach ( $match as $team => $data ) {
					if ( $team == 'nb' ) continue;

					if ( ! $team = league_get_team( $data['team'], 'link', get_post_meta( $atts['seasonid'], '_season_year', TRUE ) ) ) {
						break;
					}

					$body .= '<td class="team">' . $team . '</td>';

					$sets[] = $data['sets'];

					if ( !( $settings['winningsets'] > 1 && strpos( $data['points'], '+' ) === false ) ) {
						$points[] = $data['points'];
					}

					// home team win, draw or defeat
					if ( strpos( $team, '<a' ) === 0 || $status ) {
						if ( count( $sets ) == 1 ) {
							$status = 'foobar';
						} elseif ( $sets[0] == $sets[1] ) {
							$status = 'warning';
						} else {
							if ( strpos( $team, '<a' ) === 0 ) {
								if ( $sets[0] < $sets[1] ) {
									if ( get_post_type() == 'team' ) {
										if ( $data['team'] == 'p' . get_the_ID() ) $status = 'ok';
										else $status = 'error';
									}
									else $status = ( $status ? 'error-' : '' ) . 'ok';
								} else {
									if ( get_post_type() == 'team' ) {
										if ( $data['team'] == 'p' . get_the_ID() ) $status = 'error';
										else $status = 'ok';
									}
									else $status = ( $status ? 'ok-' : '' ) . 'error';
								}
							} else {
								if ( $sets[0] > $sets[1] ) {
									$status = 'ok';
								} else {
									$status = 'error';
								}
							}
						}
					}
				}

				if ( $atts['hometeams'] && $status == 'warning' && ! is_numeric( $sets[0] ) ) {
					$status = '';
				}
				if ( ! array_filter( $sets ) ) {
					$sets = array_fill( 0, 2, '&ndash;' );
				}

				$body .= '<td class="score' . ( $status ? ' ' . $status : '' ) . '"'
				         . ( !empty( $points ) ? ' data-points="' . implode( ',', $points ) . '"' : '' ) . '><span>' . implode( '&nbsp;:&nbsp;', $sets ) . '</span></td>';
				$body .= '</tr>';
			}
		}
	} else {
		$body .= '<tr><td colspan="6">' . __( 'No schedule available.', 'league' ) . '</td></tr>';
	}

	$body .= '</tbody>';

	$content = '<table class="season-schedule">'
	           . ( ! empty( $caption ) ? $caption : '' )
	           . $header
	           . $body
	           . '</table>';

	return $content;
}

//add_shortcode( 'league_season_schedule', 'league_season_schedule_shortcode' );
//function league_season_schedule_shortcode( $atts = array() ) {
//	if ( empty( $atts ) ) {
//		$atts = array();
//	}
//	$atts += array(
//		'seasonid' => get_the_ID(), // season ID
//		'hometeams' => FALSE,
//		'title' => '',
//	);
//}
