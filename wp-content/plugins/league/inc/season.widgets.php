<?php

/**
 * ...
 */
class Season_Schedule_Widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      // Base ID of your widget
      'season_schedule_widget',
      // Widget name will appear in UI
      __('Season schedule', 'league'),
      // Widget description
      array( 'description' => __( "Show season's schedule.", 'league' ), )
    );
  }

  // widget frontend
  public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );

    // before and after widget arguments are defined by themes
    echo $args['before_widget'];

    if ( !empty( $title ) )
      echo $args['before_title'] . $title . $args['after_title'];

    // This is where you run the code and display the output
    echo do_shortcode( '[league_season_schedule seasonid="' . $instance['season'] . '"]' );

    echo $args['after_widget'];
  }

  // widget backend
  public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) $title = $instance[ 'title' ];
    else $title = __( '', 'league' );

    if ( isset( $instance[ 'season' ] ) ) $season = $instance[ 'season' ];
    else $season = '';

    if ( !$seasons = get_posts( 'post_type=season&post_status=publish&posts_per_page=-1' ) )
      $seasons = array();

    // Widget admin form ?>

    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>">
        <?php _e( 'Title' ); ?>:</label>
      <input class="widefat"
             id="<?php echo $this->get_field_id( 'title' ); ?>"
             name="<?php echo $this->get_field_name( 'title' ); ?>"
             type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>

    <p>
      <label for="<?php echo $this->get_field_id( 'season' ); ?>">
        <?php _e( 'Season' ); ?></label>

      <select class="widefat"
              id="<?php echo $this->get_field_id( 'season' ); ?>"
              name="<?php echo $this->get_field_name( 'season' ); ?>">
        <option value="">-- <?php echo sprintf( __( 'Choose %s', 'league' ), __( 'Season', 'league' ) ); ?> --</option>
        <?php foreach ( $seasons as $post ): ?>
          <option<?php echo ( $post->ID == $season ? ' selected="selected"' : '' ); ?> value="<?php echo $post->ID ?>"><?php echo $post->post_title ?></option>
        <?php endforeach; ?>
      </select>
    </p>

  <?php }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['season'] = ( !empty( $new_instance['season'] ) ) ? $new_instance['season'] : '';

    return $instance;
  }

}

/**
 * ...
 */
class Season_Table_Widget extends WP_Widget {

  function __construct() {
    parent::__construct(
      // Base ID of your widget
      'season_table_widget',
      // Widget name will appear in UI
      __('Season table', 'league'),
      // Widget description
      array( 'description' => __( "Show season's table.", 'league' ), )
    );
  }

  // widget frontend
  public function widget( $args, $instance ) {
    $title = apply_filters( 'widget_title', $instance['title'] );

    // before and after widget arguments are defined by themes
    echo $args['before_widget'];

    if ( !empty( $title ) )
      echo $args['before_title'] . $title . $args['after_title'];

    // This is where you run the code and display the output
    echo do_shortcode( '[league_season_table seasonid="' . $instance['season'] . '"]' );

    echo $args['after_widget'];
  }

  // widget backend
  public function form( $instance ) {
    if ( isset( $instance[ 'title' ] ) ) $title = $instance[ 'title' ];
    else $title = __( '', 'league' );

    if ( isset( $instance[ 'season' ] ) ) $season = $instance[ 'season' ];
    else $season = '';

    if ( !$seasons = get_posts( 'post_type=season&post_status=publish&posts_per_page=-1' ) )
      $seasons = array();

    // Widget admin form ?>

    <p>
      <label for="<?php echo $this->get_field_id( 'title' ); ?>">
        <?php _e( 'Title' ); ?>:</label>
      <input class="widefat"
             id="<?php echo $this->get_field_id( 'title' ); ?>"
             name="<?php echo $this->get_field_name( 'title' ); ?>"
             type="text" value="<?php echo esc_attr( $title ); ?>" />
    </p>

    <p>
      <label for="<?php echo $this->get_field_id( 'season' ); ?>">
        <?php _e( 'Season' ); ?></label>

      <select class="widefat"
              id="<?php echo $this->get_field_id( 'season' ); ?>"
              name="<?php echo $this->get_field_name( 'season' ); ?>">
        <option value="">-- <?php echo sprintf( __( 'Choose %s', 'league' ), __( 'Season', 'league' ) ); ?> --</option>
        <?php foreach ( $seasons as $post ): ?>
          <option<?php echo ( $post->ID == $season ? ' selected="selected"' : '' ); ?> value="<?php echo $post->ID ?>"><?php echo $post->post_title ?></option>
        <?php endforeach; ?>
      </select>
    </p>

  <?php }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    $instance = array();
    $instance['title'] = ( !empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
    $instance['season'] = ( !empty( $new_instance['season'] ) ) ? $new_instance['season'] : '';

    return $instance;
  }

}

// Register and load the widget
add_action( 'widgets_init', 'season__widgets_init' );
function season__widgets_init() {
  register_widget( 'season_schedule_widget' );
  register_widget( 'season_table_widget' );
}
