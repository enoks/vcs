<?php

/**
 * ...
 */
add_action( 'admin_init', 'league_member__admin_init' );
function league_member__admin_init() {
	if ( ! $role = get_role( 'editor' ) ) {
		return;
	}

	$role->add_cap( 'edit_users' );
	$role->add_cap( 'delete_users' );
	$role->add_cap( 'create_users' );
	$role->add_cap( 'list_users' );
	$role->add_cap( 'remove_users' );
	$role->add_cap( 'add_users' );
}

/**
 * Add meta boxes on the Season edit screens.
 */
add_action( 'add_meta_boxes', 'league_player__add_meta_boxes' );
function league_player__add_meta_boxes() {
	// ...
	add_meta_box(
		'season_playerdiv',
		__( 'Current squad', 'league' ),
		'league_player__meta_box',
		'team',
		'normal'
	);

	// ...
	add_meta_box(
		'season_playerdiv',
		__( 'Players of this season', 'league' ),
		'league_player__meta_box',
		'season',
		'normal'
	);
}

/**
 * Enqueue scripts and styles.
 */
// backend
add_action( 'admin_enqueue_scripts', 'league_member__admin_enqueue_scripts', 10, 1 );
function league_member__admin_enqueue_scripts( $hook ) {
	global $post;

	if ( ! in_array( $hook, array(
			'post-new.php',
			'post.php'
		) ) || ! in_array( $post->post_type, array(
			'season',
			'team'
		) )
	) {
		return;
	}

	wp_enqueue_script( 'member-admin', WP_PLUGIN_URL . '/league/js/member.admin.js' );
	wp_enqueue_style( 'member-admin', WP_PLUGIN_URL . '/league/css/member.admin.css' );
}

// frontend
add_action( 'wp_enqueue_scripts', 'league_member__enqueue_scripts' );
function league_member__enqueue_scripts() {
	wp_enqueue_style( 'member', WP_PLUGIN_URL . '/league/css/member.css' );
	wp_enqueue_script( 'member-grid', WP_PLUGIN_URL . '/league/js/member.js' );
}

/**
 * Member box content.
 */
function league_player__meta_box( $post, $teams ) {
	if ( is_numeric( $post ) ) {
		$post = get_post( $post );
	}

	// add a nonce field so we can check for it later
	wp_nonce_field( 'league_season__save_post', 'season_player_nonce' );

	// get saved/default value
	if ( ! $players = get_post_meta( $post->ID, "_{$post->post_type}_player", TRUE ) ) {
		$players = array();
	}

	// locate member template
	if ( ! $located = locate_template( array( 'member.tpl.php' ) ) ) {
		$located = WP_PLUGIN_DIR . '/league/templates/member.tpl.php';
	}

	// get all users
	$members = get_users();
	// sort by last name
	usort( $members, function ( $a, $b ) {
		return get_user_meta( $a->data->ID, 'last_name' ) > get_user_meta( $b->data->ID, 'last_name' );
	} );

	if ( $post->post_type == 'season' ) {
		// ajax (reload boxes)
		if ( ! is_array( $teams ) ) {
			$teams = array_filter( explode( ',', $teams ) );
		}
		else $teams = get_post_meta( $post->ID, '_season_team', TRUE );
	}
	// post_type == 'team'
	else {
		$teams = array( $post->ID );
		$players = array( $post->ID => $players );
	}

	if ( $teams ) {
		foreach ( $teams as $nb => $team ) {
			$input_name = $post->post_type . '_player';

			if ($post->ID != $team) {
				$input_name .= '[' . $team . ']';

				echo ($nb ? '<hr />' : '');
				echo '<h3>' . sprintf( __( 'Players of team %s', 'league' ), '<i>' . league_get_team('p' . $team, 'name') . '</i>' ) . ':</h3>';
				echo '<p><a data-team="' . $team . '" href="' . admin_url( $_SERVER['REQUEST_URI'] ) . '" class="button set-team-squad">' . __( 'Take players of the current squad', 'league' ) . '</a></p>';
			}

			foreach ( $members as $member ) {
				// no admin
				if ( $member->ID == 1 ) {
					continue;
				}

				$in_squad = isset( $players[$team] ) && array_key_exists( $member->ID, $players[$team] );

				echo '<input ' . checked( $in_squad, TRUE, FALSE ) . ' type="checkbox" name="' . $input_name . '[' . $member->data->ID . ']" value="" 1 />';
				include( $located );
			}
		}
	}
	else echo __( 'Please choose teams first.', 'league' );
}

/**
 * When the post is saved, save league data.
 *
 * @param int $post_id The ID of the post being saved.
 */
add_action( 'save_post', 'league_member__save_post' );
function league_member__save_post( $post_id ) {
	// check the user's permissions
	if ( isset( $_POST['post_type'] ) && in_array( $_POST['post_type'], array(
			'season',
			'team'
		) )
	) {
		if ( ! current_user_can( ( $_POST['post_type'] == 'team' ? 'edit_team' : 'edit_page' ), $post_id ) ) {
			return;
		}
	} else {
		return;
	}

	// if this is an autosave, our form has not been submitted, so we don't want to do anything
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	$key = $_POST['post_type'] . '_player';

	$value = $_POST[ $key ];

	// update the meta field in the database
	update_post_meta( $post_id, "_{$key}", $value );
}

/**
 * Add schedule.
 *
 * @uses is_single()
 */
add_filter( 'the_content', 'league_member__the_content', 30 );
function league_member__the_content( $content ) {
	if ( is_single() && in_array( get_post_type(), array( 'team', 'season' ) ) ) {
		if ( $players = get_post_meta( get_the_ID(), '_' . get_post_type() . '_player', TRUE ) ) {
			// locate member template
			if ( ! $located = locate_template( array( 'member.tpl.php' ) ) ) {
				$located = WP_PLUGIN_DIR . '/league/templates/member.tpl.php';
			}

			if ( get_post_type() == 'team' ) {
				$players = array( $players );
			}

			foreach ( $players as $team => $squad ) {
				$content .= '<h2>' . __( 'Squad', 'league' )
				            . ( $team ? ' <a href="' . get_permalink( $team ) . '">' . league_get_team('p' . $team, 'name') . '</a>' : '' ) . '</h2>';
				$content .= '<div class="members">';

				foreach ( $squad as $uid => $member ) {
					if ( ! $member = get_user_by( 'id', $uid ) ) {
						continue;
					}

					ob_start();
					include( $located );
					$content .= ob_get_clean();
				}

				$content .= '</div>';
			}
		}
	}

	// Returns the content.
	return $content;
}
