<?php

/**
 * Register season post type.
 */
add_action( 'init', 'league_team__init' );
function league_team__init() {
	// home teams post type
	register_post_type( 'team', array(
		'labels' => array(
			'name' => __( 'Home Teams', 'league' ),
			'singular_name' => __( 'Team', 'league' ),
			'search_items' => __( 'Search Teams', 'league' ),
			'all_items' => __( 'Home Teams', 'league' ),
			'edit_item' => __( 'Edit Team', 'league' ),
			'add_new' => __( 'Add New Team', 'league' ),
			'view_item' => __( 'View Team', 'league' ),
		),
		'has_archive' => TRUE,
		'public' => TRUE,
		'rewrite' => array(
			'slug' => 'team'
		),
		'capability_type' => array( 'team', 'teams' ),
	) );

	// register team taxonomy
	register_taxonomy( 'team', 'season', array(
//    'show_admin_column' => true,
		'hierarchical' => TRUE,
		'labels' => array(
			'name' => __( 'Teams', 'league' ),
			'singular_name' => __( 'Team', 'league' ),
			'search_items' => __( 'Search Teams', 'league' ),
			'all_items' => __( 'All Teams', 'league' ),
			'edit_item' => __( 'Edit Team', 'league' ),
			'update_item' => __( 'Update Team', 'league' ),
			'add_new_item' => __( 'Add New Team', 'league' ),
			'new_item_name' => __( 'New Team Name', 'league' ),
			'menu_name' => __( 'Teams', 'league' ),
		),
		'capabilities' => array(
			'manage_terms' => 'manage_teams',
			'edit_terms' => 'manage_teams',
			'delete_terms' => 'delete_team',
			'assign_terms' => 'assign_teams',
		),
	) );
}

/**
 * ...
 */
add_action( 'admin_init', 'league_team__admin_init' );
function league_team__admin_init() {
	foreach ( array( 'administrator', 'editor' ) as $role ) {
		if ( ! $role = get_role( $role ) ) {
			continue;
		}

		$role->add_cap( 'edit_team' );
		$role->add_cap( 'read_team' );
		$role->add_cap( 'delete_team' );
		$role->add_cap( 'edit_teams' );
		$role->add_cap( 'edit_others_teams' );
		$role->add_cap( 'publish_teams' );
		$role->add_cap( 'read_private_teams' );
		$role->add_cap( 'delete_teams' );
		$role->add_cap( 'delete_private_teams' );
		$role->add_cap( 'delete_published_teams' );
		$role->add_cap( 'delete_others_teams' );
		$role->add_cap( 'edit_private_teams' );
		$role->add_cap( 'edit_published_teams' );

		$role->add_cap( 'manage_teams' );
		$role->add_cap( 'delete_team' );
		$role->add_cap( 'assign_teams' );

		if ( $role->name != 'administrator' ) {
			$role->remove_cap( 'delete_team' );
			$role->remove_cap( 'delete_teams' );
			$role->remove_cap( 'delete_private_teams' );
			$role->remove_cap( 'delete_published_teams' );
			$role->remove_cap( 'delete_others_teams' );
		}
	}
}

/**
 * Add meta boxes on the team edit screens.
 */
add_action( 'add_meta_boxes', 'league_team__add_meta_boxes' );
function league_team__add_meta_boxes() {
	add_meta_box(
		"team_rankingsdiv",
		__( '<em>Additional</em> past rankings', 'league' ) . ' <span style="font-weight:normal;">(' . __( 'not available as a season', 'league' ) . ')</span>',
		"team_rankings__meta_box",
		'team',
		'normal'
	);
}

/**
 * Rankings box content.
 */
function team_rankings__meta_box( $post ) {
	// add a nonce field so we can check for it later
	wp_nonce_field( 'league_team__save_post', 'team_rankings_nonce' );

	// get saved/default value
	$value = get_post_meta( $post->ID, '_team_rankings', TRUE );

	echo '<textarea id="team_rankings" name="team_rankings" style="width:100%;">' . $value . '</textarea>';

	echo '<p>' . __( 'Format: &lt;li&gt;SEASON - LEAGUE / &lt;strong&gt;RANKING NB&lt;/strong&gt; of CNT TEAMS / &lt;em&gt;NOTE&lt;/em&gt;&lt;/li&gt;', 'league' ) . '</p>';
}

/**
 * When the post is saved, save league data.
 *
 * @param int $post_id The ID of the post being saved.
 */
add_action( 'save_post', 'league_team__save_post' );
function league_team__save_post( $post_id ) {
	// check the user's permissions
	if ( isset( $_POST['post_type'] ) && 'team' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_team', $post_id ) ) {
			return;
		}
	} else {
		return;
	}

	// if this is an autosave, our form has not been submitted, so we don't want to do anything
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// check if our nonce is set and verify that the nonce is valid
	if ( ! isset( $_POST['team_rankings_nonce'] ) || ! wp_verify_nonce( $_POST['team_rankings_nonce'], 'league_team__save_post' ) ) {
		return;
	}

	/* OK, it's safe for us to save the data now. */

	// make sure that it is set
	if ( ! isset( $_POST['team_rankings'] ) ) {
		return;
	}

	// update the meta field in the database
	update_post_meta( $post_id, '_team_rankings', $_POST['team_rankings'] );
}

/**
 * Hide parent category from team's taxonomy.
 */
add_action( 'admin_head-edit-tags.php', 'league_team_edit_tags' );
function league_team_edit_tags() {
	if ( ! in_array( $_GET['taxonomy'], array( 'team' ) ) ) {
		return;
	}
	if ( is_admin() ) {
		$parent = 'parent()';
	}
	if ( isset( $_GET['action'] ) ) {
		$parent .= '.parent()';
	} ?>

	<script type="text/javascript" id="foo">
		(function ($) {
			$(document).ready(function () {
				$('label[for=parent]').<?php echo $parent; ?>.hide();
			});
		})(jQuery);
	</script>

<?php }

// team autocomplete
add_action( 'wp_ajax_league-team', 'league_team_autocomplete' );
add_action( 'wp_ajax_nopriv_league-team', 'league_team_autocomplete' );

function league_team_autocomplete() {
	if ( ! isset( $_GET['q'] ) ) {
		wp_die( 0 );
	}

	global $wpdb;

	// team posts (home teams)
	$query = 'SELECT post_title, id FROM ' . $wpdb->posts .
	         ' WHERE post_title LIKE "%' . $_GET['q'] . '%"' .
	         ' AND post_type = "team"' .
	         ' AND post_status = "publish"' .
	         ' ORDER BY post_title ASC;';

	$results = array();
	foreach ( $wpdb->get_results( $query ) as $result ) {
		$results[ 'p' . $result->id ] = $result->post_title;
	}

	// get taxonomy teams
	foreach (
		get_terms( 'team', array(
			'name__like' => $_GET['q'],
			'hide_empty' => FALSE
		) ) as $team
	) {
		$results[ 't' . $team->term_id ] = $team->name;
	}

	sort( $results );

	echo join( $results, "\n" );
	wp_die();
}

/**
 * ...
 */
function league_get_team( $id = '', $return = OBJECT, $year = null ) {
	if ( empty( $id ) ) {
		$team = '';
	} elseif ( $return == 'id' ) {
		if ( $team = get_page_by_title( $id, OBJECT, 'team' ) ) {
			$team = 'p' . $team->ID;
		} else {
			if ( ! $team = get_term_by( 'name', $id, 'team', ARRAY_A ) ) // insert team on the fly
			{
				$team = wp_insert_term( $id, 'team' );
			}

			$team = 't' . $team['term_id'];
		}
	} elseif ( preg_match( '/(p|t)(\d+)/', $id, $team ) ) {
		if ( $team[1] == 'p' ) {
			if ( $team = get_post( $team[2] ) ) {
				if ( $return == 'name' ) {
					$team = $team->post_title;
				} elseif ( $return == 'link' ) {
					if ( is_null( $year ) ) {
						$year = (int)date( 'Y' );
					} else {
						$year = (int)substr($year, 0, -3);
					}

					$team = '<a href="' . get_permalink( $team->ID ) . '"><strong>'
					        . ( $year >= 2015 ? 'VCS' : 'RSV' ) . ' ' . $team->post_title . '</strong></a>';
				}
			}
		} elseif ( $team = get_term( $team[2], 'team' ) ) {
			if ( $return != OBJECT ) {
				$team = $team->name;
			}
		}
	}

	return $team;
}

/**
 * Add schedule stuff to team detail.
 *
 * @uses is_single()
 */
add_filter( 'the_content', 'league_team__the_content', 20 );
function league_team__the_content( $content ) {
	if ( is_single() && get_post_type() == 'team' ) {
		// find all team seasons
		$the_query = new WP_Query( array(
			'post_type' => 'season',
			'meta_key' => '_season_year',
			'orderby' => 'meta_value',
			'meta_query' => array(
				array(
					'key' => '_season_team',
					'value' => get_the_ID(),
					'compare' => 'LIKE',
				),
			)
		) );

		// loop
		if ( $the_query->have_posts() ) {
			$seasons = array();
			while ( $the_query->have_posts() ) {
				$the_query->the_post();
				$seasons[] = get_post( get_the_ID() );
			}
		}

		// restore original Post Data
		wp_reset_postdata();

		if ( $seasons ) {
			$content .= '<h2>' . __( 'Past rankings', 'league' ) . '</h2>';
			$content .= '<ul>';
			foreach ( $seasons as $season ) {
				$note = get_post_meta( $season->ID, '_season_note', TRUE );
				$rank  = NULL;

				// get rank (key + 1)
				if ($table = get_post_meta( $season->ID, '_season_table', TRUE )) {
					foreach ( $table as $i => $team ) {
						if ( $team['team'] == 'p' . get_the_ID() ) {
							$rank = $i ++;
							break;
						}
					}
				}

				$content .= '<li><a href="' . get_permalink( $season->ID ) . '">' . $season->post_title . '</a>'
				            . ( ! is_null( $rank ) ? ' / ' . sprintf( __( '<strong>Rank %d</strong> of %d teams', 'league' ), $rank + 1, count( $table ) ) : '' )
				            . ( !empty( $note[get_the_ID()] ) ? ' / <em>' . $note[get_the_ID()] . '</em>' : '' ) . '</li>';
			}

            // additional past rankings
            if ( $rankings = get_post_meta( get_the_ID(), '_team_rankings', TRUE ) ) {
                $content .= $rankings;
            }

			$content .= '</ul>';

			// table
			$content .= do_shortcode( '[league_season_table seasonid="' . $seasons[0]->ID . '" title="' . __( 'Current table', 'league' ) . '"]' );
			$content .= do_shortcode( '[league_season_schedule seasonid="' . $seasons[0]->ID . '" title="' . __( 'Current schedule', 'league' ) . '" hometeams="true"]' );
		}
	}

	// Returns the content.
	return $content;
}
