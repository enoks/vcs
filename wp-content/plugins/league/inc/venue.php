<?php

/**
 * Register taxonomy tags.
 */
//add_action( 'init', 'league_venue__init' );
function league_venue__init() {
  // register venue taxonomy
  register_taxonomy( 'venue', 'season', array(
    'labels' => array(
      'name' => __( 'Venues', 'league' ),
      'singular_name' => __( 'Venue', 'league' ),
      'search_items' =>  __( 'Search Venues', 'league' ),
      'all_items' => __( 'All Venues', 'league' ),
      'edit_item' => __( 'Edit Venue', 'league' ),
      'update_item' => __( 'Update Venue', 'league' ),
      'add_new_item' => __( 'Add New Venue', 'league' ),
      'new_item_name' => __( 'New Venue Name', 'league' ),
      'menu_name' => __( 'Venues', 'league' ),
    ),
    'rewrite' => array(
      'slug' => 'spielort',
    ),
  ) );
}

/**
 * Hide parent category from league's taxonomy.
 */
add_action( 'admin_head-edit-tags.php', 'league_venue__edit_tags' );
function league_venue__edit_tags() {
  if ( !in_array( $_GET['taxonomy'], array( 'venue' ) ) ) return;

  $parent = 'parent()';
  if ( isset( $_GET['action'] ) ) $parent .= '.parent()'; ?>

  <script type="text/javascript" id="foo">
    ( function( $ ) {
      $(document).ready( function() {
        $( 'label[for=tag-slug]' ).<?php echo $parent; ?>.hide();
      } );
    } )( jQuery );
  </script>

<?php }
