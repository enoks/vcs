<?php

/**
 * Register season post type.
 */
add_action( 'init', 'league_season__init' );
function league_season__init() {
	register_post_type( 'season', array(
		'labels' => array(
			'name' => __( 'Seasons', 'league' ),
			'singular_name' => __( 'Season', 'league' ),
			'search_items' => __( 'Search Seasons', 'league' ),
			'all_items' => __( 'Seasons', 'league' ),
			'edit_item' => __( 'Edit Season', 'league' ),
			'add_new' => __( 'Add New Season', 'league' ),
			'view_item' => __( 'View Season', 'league' ),
		),
		'menu_icon' => 'dashicons-groups',
		'menu_position' => 5,
		'has_archive' => TRUE,
		'taxonomies' => array( 'league' ),
		'public' => TRUE,
		'rewrite' => array(
			'slug' => 'saison'
		)
	) );
}

/**
 * List table.
 */
add_filter( 'manage_edit-season_columns', 'season_columns' );
function season_columns( $columns ) {
	// add hometeam column
	$columns['team'] = __( 'Home Teams', 'league' );

	// rearrange columns (date at the end)
	$date = $columns['date'];
	unset( $columns['date'] );
	$columns['date'] = $date;

	return $columns;
}

/**
 * List table columns content.
 */
add_action( 'manage_season_posts_custom_column', 'season_columns_column', 10, 2 );
function season_columns_column( $column_name, $id ) {
	if ( $column_name == 'team' && ( $teams = get_post_meta( $id, '_season_team', TRUE ) ) ) {
		foreach ( $teams as &$team ) {
			$team = get_post( $team );
			$team = '<a href="' . get_permalink( $team->ID ) . '">' . $team->post_title . '</a>';
		}

		echo implode( ', ', $teams );
	}
}

/**
 * Enqueue scripts and styles.
 */
// backend
add_action( 'admin_enqueue_scripts', 'league_season__admin_enqueue_scripts', 10, 1 );
function league_season__admin_enqueue_scripts( $hook ) {
	global $post;

	if ( ! in_array( $hook, array( 'post-new.php', 'post.php' ) ) || $post->post_type != 'season' ) {
		return;
	}

	wp_register_script( 'jquery-ui-i18n', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/i18n/jquery-ui-i18n.min.js' );
	wp_enqueue_script( 'season', WP_PLUGIN_URL . '/league/js/season.admin.js', array(
		'suggest',
		'jquery-ui-datepicker',
		'jquery-ui-i18n',
		'jquery-ui-sortable'
	) );
	wp_localize_script( 'season', 'league', array( 'siteurl' => get_option( 'siteurl' ) ) );

	add_thickbox();

	wp_enqueue_style( 'datepicker', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css' );
	wp_enqueue_style( 'season', WP_PLUGIN_URL . '/league/css/season.admin.css' );
}

// frontend
add_action( 'wp_enqueue_scripts', 'league_season__enqueue_scripts' );
function league_season__enqueue_scripts() {
	if ( is_admin() ) {
		return;
	}

	wp_enqueue_style( 'season', WP_PLUGIN_URL . '/league/css/season.css' );
	wp_enqueue_script( 'season', WP_PLUGIN_URL . '/league/js/season.js' );

	add_thickbox();
}

/**
 * Add meta boxes on the Season edit screens.
 */
add_action( 'add_meta_boxes', 'league_season__add_meta_boxes' );
function league_season__add_meta_boxes() {
	$side_boxes = array(
		'year' => __( 'Year' ),
		'league' => __( 'League', 'league' ),
		'note' => __( 'Season Note', 'league' ),
		'team' => __( 'Home Teams', 'league' ),
		'settings' => __( 'Settings' ),
	);

	foreach ( $side_boxes as $key => $title ) {
		add_meta_box(
			"season_{$key}div",
			$title,
			"season_{$key}__meta_box",
			'season',
			'side'
		);
	}

	// table
	add_meta_box(
		'season_tablediv',
		__( 'Table', 'league' ),
		'season_table__meta_box',
		'season',
		'normal'
	);

	// schedule
	add_meta_box(
		'season_schedulediv',
		__( 'Schedule', 'league' ),
		'season_schedule__meta_box',
		'season',
		'normal'
	);

	// remove default league taxonomy meta boxes
	remove_meta_box( 'leaguediv', 'season', 'side' );
	remove_meta_box( 'teamdiv', 'season', 'side' );
	remove_meta_box( 'tagsdiv-venue', 'season', 'side' );
}

/**
 * Year box content.
 */
function season_year__meta_box( $post ) {
	// add a nonce field so we can check for it later
	wp_nonce_field( 'league_season__save_post', 'season_year_nonce' );

	// get saved/default value
	$value = get_post_meta( $post->ID, '_season_year', TRUE );

	echo '<select id="season_year" class="required" name="season_year">';
	echo '<option value="">-- ' . sprintf( __( 'Choose %s', 'league' ), __( 'Year' ) ) . ' --</option>';
	for ( ( $year = (int) date( 'Y' ) ); $year >= 1958; $year -- ) {
		$option = $year . '/' . substr( $year + 1, 2 );
		echo '<option value="' . esc_attr( $option ) . '"'
		     . ( esc_attr( $option ) == $value ? ' selected="selected"' : '' ) . '>'
		     . $option . '</option>';
	}
	echo '</select>';
}

/**
 * League (taxonomy) box content.
 */
function season_league__meta_box( $post, $year, $league = false, $bvv_msid = false ) {
	// add a nonce field so we can check for it later
	wp_nonce_field( 'league_season__save_post', 'season_league_nonce' );

	// get saved value
	$value = $league === false ? get_post_meta( $post->ID, '_season_league', TRUE ) : $league;

	echo '<select id="season_league" class="required" name="season_league">'
	     . '<option value="">-- ' . sprintf( __( 'Choose %s', 'league' ), __( 'League', 'league' ) ) . ' --</option>';

	foreach ( get_terms( 'league', 'hide_empty=0' ) as $league ) {
		echo '<option value="' . $league->term_id . '"'
		     . ( $league->term_id == $value ? ' selected="selected"' : '' ) . '>'
		     . $league->name . '</option>';
	}
	echo '</select>';
	echo '<p class="description">' . __( 'intern' ) . '</p>';

	//---
	// BVV
	echo '<hr />';

	// add a nonce field so we can check for it later
	wp_nonce_field( 'league_season__save_post', 'season_bvv_msid_nonce' );

	if ( $season = bvv_get( 'seasons' ) ) {
		$year = (! empty( $year ) && ! is_array( $year ) ? $year : get_post_meta( $post->ID, '_season_year', true ));
		if ( $season = array_filter( ((array) $season)['season'], function( $season ) use ( $year ) {
			return $season->name == $year;
		} ) ) $season = current($season);
	}

	$bvv_msid = $bvv_msid === false ? get_post_meta( $post->ID, '_bvv_msid', true ) : $bvv_msid;
	if ( $season && ($match_series = bvv_get( 'matchSeries', array( 'seasonId' => (string) $season->id )) ) ) {
		echo '<select id="bvv-msid" name="bvv_msid">';
		echo '<option value="">-- ' . __( 'Not set' ) . ' --</option>';
		foreach ( $match_series->matchSeries as $match_series ) {
			$teams = bvv_get( 'teams', array( 'matchSeriesId' => (string) $match_series->id ) );
			$teams = array_filter( ((array) $teams)['team'], function( $team ) {
				return $team->name == 'VC Strausberg';
			} );

			if ( !$teams ) continue;

			echo '<option' . selected( $match_series->id, $bvv_msid, false ) . ' value="' . $match_series->id . '">'
			     . $match_series->name
			     . (!$year ? ' (' . $match_series->season->name . ')' : '')
			     . '</option>';
		}
		echo '</select>';
	}
	else echo '<input type="text" id="bvv-msid" name="bvv_msid" value="' . $bvv_msid . '" />';
	echo '<p class="description">' . __( 'BVV Match Series', 'league' ) . '</p>';
}

/**
 * Team box content.
 */
function season_team__meta_box( $post ) {
	// add a nonce field so we can check for it later
	wp_nonce_field( 'league_season__save_post', 'season_team_nonce' );

	// get saved value
	if ( ! $value = get_post_meta( $post->ID, '_season_team', TRUE ) ) {
		$value = array();
	}

	echo '<ul id="teamchecklist" data-wp-lists="list:team" class="categorychecklist form-no-clear">';
	if ( $teams = get_posts( 'post_type=team&post_status=publish&posts_per_page=-1&orderby=title&order=ASC' ) ) {
		foreach ( $teams as $team ) {
			echo '<li id="team-' . $team->ID . '">';
			echo '<label class="selectit">';
			echo '<input value="' . $team->ID . '" name="season_team[]" id="in-team-' . $team->ID . '" type="checkbox"';
			echo ( in_array( $team->ID, $value ) ? ' checked="checked"' : '' ) . '>';
			echo ' ' . $team->post_title;
			echo '</label></li>';
		}
	} else {
		echo '<p>...</p>';
	}

	echo '</ul>';
}

/**
 * Note box content.
 */
function season_note__meta_box( $post, $teams ) {
	if ( is_numeric( $post ) ) {
		$post = get_post( $post );
	}

	// add a nonce field so we can check for it later
	wp_nonce_field( 'league_season__save_post', 'season_note_nonce' );

	// get saved value
	if ( ! $notes = get_post_meta( $post->ID, '_season_note', TRUE ) ) {
		$notes = array();
	}

	// ajax (reload boxes)
	if ( ! is_array( $teams ) ) {
		$teams = array_filter( explode( ',', $teams ) );
	}
	else $teams = get_post_meta( $post->ID, '_season_team', TRUE );

	if ( $teams && ( $teams = array_flip( $teams ) ) ) {
		foreach ( $teams as $team => $note )
			$teams[$team] = ( isset( $notes[$team] ) ? $notes[$team] : '' );

		foreach ( $teams as $team => $note ) {
			$team = league_get_team( 'p' . $team );

			echo '<div><label for=""> ' . sprintf( __( 'Note to %s', 'league' ), $team->post_title ) . '</label>';
			echo '<input type="text" value="' . $note . '" name="season_note[' . $team->ID . ']" />';
			echo '</div>';
		}
	}
	else echo __( 'Please choose teams first.', 'league' );
}

/**
 * Settings box content.
 */
function season_settings__meta_box( $post, $winningsets, $manualtable = false, $bvv_msid = false ) {
	if ( is_numeric( $post ) ) {
		$post = get_post( $post );
	}

	if ( $bvv_msid === false ) $bvv_msid = get_post_meta( $post->ID, '_bvv_msid', true );
	if ( $bvv_msid ) {
		echo '<p>' . __( "Nothing to enter because it's a BVV season.", 'league' ) . '</p>';
		return;
	}

	// add a nonce field so we can check for it later
	wp_nonce_field( 'league_season__save_post', 'season_settings_nonce' );

	// get saved value
	$settings = get_season_settings( $post->ID );

	// ajax (reload boxes)
	if ( ! empty( $winningsets ) && ! is_array( $winningsets ) ) {
		$settings['winningsets'] = $winningsets;
		$settings['manualtable'] = $manualtable === 'true' ? TRUE : FALSE;
	}

	// needs to be an integer
	if ( $settings['winningsets'] != '' ) {
		$settings['winningsets'] = (int) $settings['winningsets'];
	}

	// winning sets
	echo '<div><input type="text" data-postid="' . $post->ID . '" value="' . $settings['winningsets'] . '" id="winning-sets" name="season_settings[winningsets]" class="numeric required" />';
	echo '<label for="winning-sets"> ' . __( 'Winning sets', 'league' );
	echo '</label></div>';

	// points
	if ( $settings['winningsets'] ) {
		// win
		echo '<p><strong>' . __( 'Points for a win', 'league' ) . '</strong></p>';
		for ( $i = 0; $i < $settings['winningsets']; $i ++ ) {
			$final_score = $settings['winningsets'] . ':' . $i;

			echo '<div><input type="text" value="' . $settings['points'][ $final_score ] . '" name="season_settings[points][' . $final_score . ']" class="numeric required" />';
			echo '<label for=""> ' . sprintf( __( 'points for %s final score', 'league' ), $final_score );
			echo '</label></div>';
		}

		// defeat
		echo '<p><strong>' . __( 'Points for a defeat', 'league' ) . '</strong></p>';
		for ( $i = $settings['winningsets'] - 1; $i >= 0; $i -- ) {
			$final_score = $i . ':' . $settings['winningsets'];

			echo '<div><input type="text" value="' . $settings['points'][ $final_score ] . '" name="season_settings[points][' . $final_score . ']" class="numeric" placeholder="0" />';
			echo '<label for=""> ' . sprintf( __( 'points for %s final score', 'league' ), $final_score );
			echo '</label></div>';
		}

		// table calculation vs manual
		echo '<hr /><div><label class="selectit">';
		echo '<input value="1" id="manual-table" name="season_settings[manualtable]" id="" type="checkbox"'
		     . ( $settings['manualtable'] ? ' checked="checked"' : '' ) . ' />';
		echo ' ' . __( 'Manual table input', 'league' );
		echo '</label></div>';

		echo '<div id="points-input" style="display:none;">';
		echo '<p>' . __( 'Enter the results for each set.<br />If you only have the total points, enter them in <i>set 1</i>.', 'league' ) . '</p>';
		echo '<table>';
		echo '<thead><tr><th>&nbsp;</th><th>...</th><th>...</th></tr></thead>';
		echo '<tbody>';
		for ( $i = 1; $i <= ($settings['winningsets'] * 2 - 1); $i ++ ) {
			echo '<tr>';
			echo '<td><label for="">' . sprintf( __( 'Set %d', 'league' ), $i ) . '</label></td>';
			echo '<td><input type="text" /> : </td>';
			echo '<td><input type="text" /></td>';
			echo '</tr>';
		}
		echo '</tbody>';
		echo '<tfoot><tr><td colspan="3">&nbsp;</td></tr><tr><td>&nbsp;</td>';
		echo '<td colspan="2"><button class="button">' . __( 'Update' ) . '</button></td></tr></tfoot>';
		echo '</table>';
		echo '</div>';
	}
}

/**
 * Get season settings (at least default values).
 */
function get_season_settings( $postid ) {
	if ( ! $settings = get_post_meta( $postid, '_season_settings', TRUE ) ) {
		$settings = array();
	}

	return $settings + array(
		'bvv_msid' => get_post_meta( $postid, '_bvv_msid', true ),
		'winningsets' => '',
		'points' => array(),
		'manualtable' => FALSE,
		'teams' => get_post_meta( $postid, '_season_team', true ),
	);
}

/**
 * Table box content.
 */
function season_table__meta_box( $post, $winningsets, $manualtable = false, $bvv_msid = false ) {
	if ( is_numeric( $post ) ) {
		$post = get_post( $post );
	}

	if ( $bvv_msid === false ) $bvv_msid = get_post_meta( $post->ID, '_bvv_msid', true );
	if ( $bvv_msid ) {
		echo '<p>' . __( "Nothing to enter because it's a BVV season.", 'league' ) . '</p>';
		return;
	}

	// add a nonce field so we can check for it later
	wp_nonce_field( 'league_season__save_post', 'season_table_nonce' );

	$settings = get_season_settings( $post->ID );

	// ajax (reload boxes)
	if ( ! empty( $winningsets ) && ! is_array( $winningsets ) ) {
		$settings['winningsets'] = $winningsets;
		$settings['manualtable'] = $manualtable === 'true' ? TRUE : FALSE;
	}

	// needs to be an integer
	if ( $settings['winningsets'] != '' ) {
		$settings['winningsets'] = (int) $settings['winningsets'];
	}

	// auto table ... stop here
	if ( ! $settings['manualtable'] ) {
		echo '<p>' . __( '... will be generated automatically from schedule.', 'league' ) . '</p>';

		return;
	}

	echo '<table class="wp-list-table widefat striped season-schedule">';

	echo '<colgroup><col class="team" />';
	echo str_repeat( '<col class="sets" />', $settings['winningsets'] * 2 );
	echo '<col class="difference" />';
	echo '<col class="delete" /></colgroup>';

	echo '<thead><tr>';
	echo '<th><strong>' . __( 'Team', 'league' ) . '</strong></th>';
	for ( $i = 0; $i < $settings['winningsets']; $i ++ ) {
		echo '<th><strong>' . $settings['winningsets'] . ':' . $i . '</strong></th>';
	}
	for ( $i = $settings['winningsets'] - 1; $i >= 0; $i -- ) {
		echo '<th><strong>' . $i . ':' . $settings['winningsets'] . '</strong></th>';
	}
	echo '<th><strong>' . __( 'Difference', 'league' ) . '</strong> (' . __( 'small pts', 'league' ) . ')</th>';
	echo '<th></th>';
	echo '</tr></thead>';

	// get saved values
	if ( ! $table = get_post_meta( $post->ID, '_season_table', TRUE ) ) {
		$table = array( default_table_team() );
	}

	echo '<tbody>';
	foreach ( $table as $nb => $team ) {
		$team['team'] = league_get_team( $team['team'], 'name' );

		// team
		echo '<tr><td><input type="text" name="table[' . $nb . '][team]" data-autocomplete="team" value="' . $team['team'] . '" class="required" placeholder="' . __( 'Team', 'league' ) . '" /></td>';

		// wins
		for ( $i = 0; $i < $settings['winningsets']; $i ++ ) {
			$result = $settings['winningsets'] . ':' . $i;
			echo '<td><input type="text" name="table[' . $nb . '][results][' . $result . ']" value="' . $team['results'][ $result ] . '" placeholder="0" /></td>';
		}

		// defeats
		for ( $i = $settings['winningsets'] - 1; $i >= 0; $i -- ) {
			$result = $i . ':' . $settings['winningsets'];
			echo '<td><input type="text" name="table[' . $nb . '][results][' . $result . ']" value="' . $team['results'][ $result ] . '" placeholder="0" /></td>';
		}

		// small points
		echo '<td><input type="text" name="table[' . $nb . '][points]" value="' . ( $team['points'][0] - $team['points'][1] ? $team['points'][0] - $team['points'][1] : '' ) . '" placeholder="0" /></td>';

		// delete row
		echo '<td><a href="#" class="delete-row" title="' . __( 'Delete' ) . '">'
		     . '<span class="dashicons dashicons-no-alt"></span></a></td></tr>';
	}
	echo '</tbody>';

	// ...
	echo '<tfoot><tr><td colspan="4"><a href="#" class="add-row"><strong>+ ' . __( 'Add Another Team', 'league' )
	     . '</strong></a></td></tr></tfoot></table>';
}

/**
 * Schedule box content.
 */
function season_schedule__meta_box( $post, $bvv_msid = false ) {
	if ( is_numeric( $post ) ) {
		$post = get_post( $post );
	}

	$bvv_msid = (! empty( $bvv_msid ) && ! is_array( $bvv_msid ) ? $bvv_msid : get_post_meta( $post->ID, '_bvv_msid', true ));
	if ( $bvv_msid ) {
		echo '<p>' . __( "Nothing to enter because it's a BVV season.", 'league' ) . '</p>';
		return;
	}

	// add a nonce field so we can check for it later
	wp_nonce_field( 'league_season__save_post', 'season_schedule_nonce' );

	// get saved schedule
	if ( ! $schedule = get_post_meta( $post->ID, '_season_schedule', TRUE ) ) {
		$schedule = array( array( array( 'nb' => '', array(), array() ) ) );
	} // structure

	// head
	echo '<table class="wp-list-table widefat striped season-schedule">';

	echo '<colgroup><col class="matchday" /><col class="number" /><col class="team" /><col class="team" /><col class="delete" /></colgroup>';
	echo '<thead><tr>';
	echo '<th><strong>' . __( 'Matchday', 'league' ) . '</strong></th>';
	echo '<th><strong>' . __( 'Game nb', 'league' ) . '</strong></th>';
	echo '<th><strong>' . sprintf( __( 'Team #%d', 'league' ), 1 ) . '</strong> / ' . __( 'small pts', 'league' ) . ' / ' . __( 'Sets', 'league' ) . '</th>';
	echo '<th><strong>' . sprintf( __( 'Team #%d', 'league' ), 2 ) . '</strong> / ' . __( 'small pts', 'league' ) . ' / ' . __( 'Sets', 'league' ) . '</th>';
	echo '<th></th>';
	echo '</tr></thead>';

	echo '<tbody>';

	// loop all matchdays
	foreach ( $schedule as $matchday => $matches ) {
		$matchday = mysql2date( 'd.m.Y', $matchday );

		// loop all matches on matchday
		foreach ( $matches as $match ) {
			echo '<tr>';
			echo '<td><input type="text" data-type="date" name="matchday[]" value="' . $matchday . '" class="required" /></td>';
			echo '<td><input type="text" name="nb[]" value="' . $match['nb'] . '" class="numeric" /></td>';

			// match
			foreach ( $match as $team => $data ) {
				if ( $team === 'nb' ) continue;

				// each team
				if ( ! $team = league_get_team( $data['team'], 'name' ) ) {
					$team = '';
				}

				echo '<td><input type="text" name="team[]" data-autocomplete="team" value="' . $team . '" class="required" placeholder="' . __( 'Team', 'league' ) . '" />'
				     . ' / <input type="text" name="points[]" value="' . $data['points'] . '" title="' . __( 'Match results', 'league' ) . '" />'
				     . ' / <input type="text" name="sets[]" value="' . $data['sets'] . '" class="numeric" /></td>';
			}

			// delete row
			echo '<td><a href="#" class="delete-row" title="' . __( 'Delete' ) . '">'
			     . '<span class="dashicons dashicons-no-alt"></span></a></td>';
			echo '</tr>';
		}
	}
	echo '</tbody>';

	// footer
	echo '<tfoot><tr><td colspan="5"><a href="#" class="add-row"><strong>+ ' . __( 'Add New Match', 'league' )
	     . '</strong></a></td></tr></tfoot></table>';
}

/**
 * When the post is saved, save league data.
 *
 * @param int $post_id The ID of the post being saved.
 */
add_action( 'save_post', 'league_season__save_post' );
function league_season__save_post( $post_id ) {
	// check the user's permissions
	if ( isset( $_POST['post_type'] ) && 'season' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}
	} else {
		return;
	}

	// if this is an autosave, our form has not been submitted, so we don't want to do anything
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	foreach ( array( 'year', 'league', 'team', 'note', 'settings' ) as $field ) {
		// check if our nonce is set and verify that the nonce is valid
		if ( ! isset( $_POST["season_{$field}_nonce"] ) || ! wp_verify_nonce( $_POST["season_{$field}_nonce"], 'league_season__save_post' ) ) {
			continue;
		}

		/* OK, it's safe for us to save the data now. */

		// make sure that it is set
		if ( ! isset( $_POST["season_{$field}"] ) ) {
			continue;
		}

		$value = $_POST["season_{$field}"];

		// sanitize user input
		if ( is_string( $value ) ) {
			$value = sanitize_text_field( $value );
		}

		// update the meta field in the database
		update_post_meta( $post_id, "_season_{$field}", $value );
	}

	// check if our nonce is set and verify that the nonce is valid
	if ( isset( $_POST["season_bvv_msid_nonce"] ) && wp_verify_nonce( $_POST["season_bvv_msid_nonce"], 'league_season__save_post' ) ) {
		// make sure that it is set
		if ( isset( $_POST["bvv_msid"] ) ) {
			// update the meta field in the database
			update_post_meta( $post_id, "_bvv_msid", $_POST["bvv_msid"] );
		}
		else delete_post_meta( $post_id, "_bvv_msid" );
	}

	$schedule = array();
	$table = array();
	$settings = get_season_settings( $post_id );
	$differenceAvailable = TRUE;

	if ( isset( $_POST['matchday'] ) ) {
		foreach ( $_POST['matchday'] as $nb => $match ) {
			if ( ! $date = date_create_from_format( 'd.m.Y H:i:s', $match . ' 00:00:00' ) ) {
				continue;
			}
			$date = date_i18n( 'Y-m-d H:i:s', date_timestamp_get( $date ) );

			$match = array(
				'nb' => $_POST['nb'][ $nb  ],
			);

			// teams + points + sets
			foreach ( array( $nb * 2, $nb * 2 + 1 ) as $i ) {
				$team = league_get_team( $_POST['team'][ $i ], 'id' );

				// schedule
				$match[ $team ] = array(
					'team' => $team,
					'points' => $_POST['points'][ $i ],
					'sets' => $_POST['sets'][ $i ],
				);

				if ( ! isset( $table[ $team ] ) ) {
					$table[ $team ] = default_table_team( array( 'team' => $team ) );
				}

				// auto table
				if ( ! $settings['manualtable'] ) {
					if ( $_POST['sets'][ $i ] != '' ) {
						$opponent = $i % 2 == 0 ? $i + 1 : $i - 1;
						$result = $_POST['sets'][ $i ] . ':' . $_POST['sets'][ $opponent ];

						if ( ! isset( $table[ $team ]['results'][ $result ] ) ) {
							$table[ $team ]['results'][ $result ] = 0;
						}
						$table[ $team ]['results'][ $result ] ++;

						$table[ $team ]['points'][0] += calculate( $_POST['points'][ $i ] );
						$table[ $team ]['points'][1] += calculate( $_POST['points'][ $opponent ] );

						// don't consider difference in sorting anymore
						if ( empty( $_POST['points'][ $i ] ) ) {
							$differenceAvailable = FALSE;
						}
					}
				}
			}

			if ( ! $match ) {
				continue;
			}

			$schedule[ $date ][] = $match;
		}

		// sort schedule by date
		ksort( $schedule );
		if ( isset( $_POST["season_schedule_nonce"] ) && wp_verify_nonce( $_POST["season_schedule_nonce"], 'league_season__save_post' ) ) {
			update_post_meta( $post_id, "_season_schedule", $schedule );
		}
	}

	// manual table
	if ( $settings['manualtable'] ) {
		$table = $_POST['table'];

		foreach ( $table as &$team ) {
			$team = default_table_team( $team );

			$team['team'] = league_get_team( $team['team'], 'id' );

			// points
			if ( (int) $team['points'] > 0 ) {
				$team['points'] = array( (int) $team['points'], 0 );
			} else {
				$team['points'] = array( 0, (int) $team['points'] * - 1 );
			}
			if ( ! is_array( $team['points'] ) ) {
				$team['points'] = array( 0, 0 );
			}
		}
	}

	// calculate 'missing' team table data
	// doesn't matter if manual or automatic
	foreach ( $table as &$team ) {
		foreach ( $team['results'] as $result => $cnt ) {
			$sets = explode( ':', $result );

			$team['matches'] += (int) $cnt;

			// wins
			if ( $sets[0] > $sets[1] ) {
				$team['wins'] += (int) $cnt;
			} // defeats
			elseif ( $sets[0] < $sets[1] ) {
				$team['defeats'] += (int) $cnt;
			} else {
				$team['draws'] ++;
			} // draws

			// sets
			foreach ( $sets as $nb => $set ) {
				$team['sets'][ $nb ] += (int) $cnt * (int) $sets[ $nb ];
			}

			// increase score
			$team['score'] += (int) $settings['points'][ $result ] * (int) $cnt;
		}
	}

	// sort table
	usort( $table, function ( $a, $b ) use ( $differenceAvailable ) {
		// no matches
		if ( empty( $a['matches'] ) && empty( $b['matches'] ) ) return 0;
		elseif ( empty( $a['matches'] ) && !empty( $b['matches'] ) ) return 1;
		elseif ( !empty( $a['matches'] ) && empty( $b['matches'] ) ) return -1;

		// sort by score
		if ( ! $sort = $b['score'] - $a['score'] ) {
			// ... by set difference
			if ( ! $sort = ( $b['sets'][0] - $b['sets'][1] ) - ( $a['sets'][0] - $a['sets'][1] ) ) // ... by sets won
			{
				if ( ! $sort = $b['sets'][0] - $a['sets'][0] ) // ... and by points difference
				{
					if ( $differenceAvailable ) {
						$sort = ( $b['points'][0] - $b['points'][1] ) - ( $a['points'][0] - $a['points'][1] );
					}
				}
			}
		}

		return $sort;
	} );

	if ( isset( $_POST["season_table_nonce"] ) && wp_verify_nonce( $_POST["season_table_nonce"], 'league_season__save_post' ) ) {
		update_post_meta( $post_id, "_season_table", $table );
	}
}

function default_table_team( $team = array() ) {
	if ( ! is_array( $team ) ) {
		$team = array();
	}

	return $team + array(
		'team' => '',
		'matches' => 0,
		'sets' => array( 0, 0 ),
		'points' => array( 0, 0 ),
		'results' => array(),
		'wins' => 0,
		'draws' => 0,
		'defeats' => 0,
		'score' => 0
	);
}

/**
 * Add schedule.
 *
 * @uses is_single()
 */
add_filter( 'the_content', 'league_season__the_content', 20 );
function league_season__the_content( $content ) {
	if ( is_single() && get_post_type() == 'season' ) {
		$content .= do_shortcode( '[league_season_table title="' . __( 'Table', 'league' ) . '"]' );
		$content .= do_shortcode( '[league_season_schedule title="' . __( 'Schedule', 'league' ) . '"]' );
	}

	// Returns the content.
	return $content;
}
