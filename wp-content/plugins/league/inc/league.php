<?php

/**
 * Register taxonomy tags.
 */
add_action( 'init', 'league__init' );
function league__init() {
  // register league taxonomy
  register_taxonomy( 'league', 'season', array(
//    'show_admin_column' => true,
    'hierarchical' => true,
    'labels' => array(
      'name' => __( 'Leagues', 'league' ),
      'singular_name' => __( 'League', 'league' ),
      'search_items' =>  __( 'Search Leagues', 'league' ),
      'all_items' => __( 'All Leagues', 'league' ),
      'edit_item' => __( 'Edit League', 'league' ),
      'update_item' => __( 'Update League', 'league' ),
      'add_new_item' => __( 'Add New League', 'league' ),
      'new_item_name' => __( 'New League Name', 'league' ),
      'menu_name' => __( 'Leagues', 'league' ),
    ),
    'rewrite' => array(
      'slug' => 'liga',
    ),
  ) );
}

/**
 * Hide parent category from league's taxonomy.
 */
add_action( 'admin_head-edit-tags.php', 'league_league_edit_tags' );
function league_league_edit_tags() {
  if ( !in_array( $_GET['taxonomy'], array( 'league' ) ) ) return;

  $parent = 'parent()';
  if ( isset( $_GET['action'] ) ) $parent .= '.parent()'; ?>

  <script type="text/javascript" id="foo">
    ( function( $ ) {
      $(document).ready( function() {
        $( 'label[for=parent], label[for=tag-slug]' ).<?php echo $parent; ?>.hide();
      } );
    } )( jQuery );
  </script>

<?php }
