( function( $, window, document ) {

    $( document).on( 'ready', function() {

        var $playerdiv = $( 'div[id$="_playerdiv"]');

        // de-/select players
        $playerdiv.on( 'click', 'article.member', function() {
            var $member = $( this );

            $member.prev().prop( 'checked', !$member.hasClass( 'checked' ) );
            $member.toggleClass( 'checked' );
        });

        // update players box
        var $teams = $( 'input[name="season_team[]"]' ).on( 'change', function() {
            var oArgs = {
                    post: $( '#winning-sets' ).data( 'postid' ),
                    teams: []
                },
                $inside = $( '#season_playerdiv').find( 'div.inside').addClass('loading');

            $teams.filter( ':checked' ).each( function() {
                oArgs.teams.push( $( this ).val() );
            } );
            oArgs.teams = oArgs.teams.join( ',' );

            // reload note box
            emit( {
                'function': 'league_player__meta_box',
                args: oArgs
            }, function( data ) {
                $inside.html( data ).removeClass('loading');
            } );
        } );

        // set players from team squad
        $playerdiv.on( 'click', 'a.set-team-squad', function( e ) {
            e.preventDefault();

            var $checkedTeams = $teams.filter( ':checked' );
            if ( !$checkedTeams.length ) return;

            var teamID = $( this ).data( 'team' ),
                $inside = $( '#season_playerdiv' ).find( 'div.inside' ).addClass( 'loading' );

            // remove all selection
            $( 'input[name^="season_player[' + teamID + '][' )
                .next( 'article' ).addClass( 'checked' ).trigger( 'click' );

            $checkedTeams.each(function() {
                emit( {
                    'function': 'get_post_meta',
                    args: {
                        post_id: teamID,
                        key: '_team_player',
                        single: true
                    }
                }, function(oPlayers) {
                    $inside.removeClass( 'loading' );

                    if ( !oPlayers ) return;

                    // select player
                    for ( var $member_id in JSON.parse( oPlayers ) ) {
                        $( 'input[name="season_player[' + teamID + '][' + $member_id + ']"]' )
                            .next( 'article.member-' + $member_id ).removeClass( 'checked' ).trigger( 'click' );
                    }
                } );
            });
        } );

    } );

} )( jQuery, window, window.document );
