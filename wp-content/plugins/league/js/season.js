( function( window, document, undefined ) {

    document.addEventListener( "DOMContentLoaded", function() {

        var $schedules = document.getElementsByClassName( 'season-schedule'),
            $schedule, $scores, i, j, k, points, result;

        for ( i = 0; i < $schedules.length; i++ ) {
            $schedule = $schedules[i].getElementsByTagName( 'tbody' );
            if ( !$schedule.length ) continue;

            $scores = $schedule[0].getElementsByClassName( 'score' );
            for ( j = 0; j < $scores.length; j++ ) {

                $scores[j].addEventListener( 'click', function() {
                    points = this.getAttribute( 'data-points' );
                    if ( !points ) return;

                    result = this.textContent.split(':');

                    points = points.split( ',' );
                    points[0] = points[0].split( '+' );
                    points[1] = points[1].split( '+' );

                    // open modal
                    tb_show( 'Satzergebnisse', "#TB_inline" );

                    var content = document.getElementById( 'TB_ajaxContent'),
                        matchDetail = '<table id="set-results">';

                    // thead
                    matchDetail += '<thead><tr>';
                    matchDetail += '<th></th>';
                    matchDetail += '<th>' + this.previousSibling.previousSibling.textContent + '</th>';
                    matchDetail += '<th style="padding-right:0; padding-left:0; width:0;">:</th>';
                    matchDetail += '<th>' + this.previousSibling.textContent + '</th>';
                    matchDetail += '</tr></thead>';

                    // tbody
                    matchDetail += '<tbody>';
                    for ( k = 0; k < points[0].length; k++ ) {
                        matchDetail += '<tr><td>Satz ' + (k+1) + '</td>';
                        matchDetail += '<td>' + points[0][k] + '</td>';
                        matchDetail += '<td>:</td>';
                        matchDetail += '<td>' + points[1][k] + '</td></tr>';
                    }

                    // result
                    matchDetail += '<tr class="' + this.className + '">'
                    + '<td>Ergebnis</td>'
                    + '<td>' + result[0] + '</td>'
                    + '<td>:</td>'
                    + '<td>' + result[1] + '</td>'
                    + '</tr>';

                    matchDetail += '</tbody>';

                    content.innerHTML = matchDetail
                    + '</table>';
                } );
            }
        }

    } );

} )( this, this.document );