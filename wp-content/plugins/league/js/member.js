( function( window, undefined ) {

    /**
     * Do stuff when DOM is loaded.
     */
    document.addEventListener( "DOMContentLoaded", function() {

        // members
        ( function() {

            var grids = document.getElementsByClassName( 'members' );
            if ( !grids.length ) return;

            // init
            for ( var g = 0; g < grids.length; g++ ) {
                if ( membersGrid.apply( grids[g] ) )
                    window.addEventListener( 'resize', membersGrid.bind( grids[g] ) );
            }

            function membersGrid() {
                var members = this.getElementsByTagName( 'article' );
                if ( !members.length ) return false;

                var columnWidth = Math.floor( members[0].getBoundingClientRect().width ),
                    left = 0, top = 0, columns = [0], column;

                for ( var c = 1; c < Math.floor( Math.round( this.offsetWidth ) / columnWidth ); c++ )
                    columns.push(0);

                // reset grid height
                this.style.height = '';

                // loop posts
                for ( var m = 0; m < members.length; m++ ) {
                    // only one column no positioning
                    if ( columns.length < 2 ) {
                        members[m].style.top = '';
                        members[m].style.left = '';
                        continue;
                    }

                    // get smallest column
                    top = Math.min.apply( Math, columns );
                    column = columns.indexOf( top );

                    // add gap
                    //if ( top ) top += iGap/(columns.length < 2 ? 4 : 2);
                    left = column * columnWidth;

                    // eventually positioning project
                    members[m].style.top = top + 'px';
                    members[m].style.left = left + 'px';

                    // 'cache' new column height
                    columns[column] = top + members[m].offsetHeight;

                    // adjust grid's height
                    if ( this.offsetHeight < columns[column] )
                        this.style.height = columns[column] + 'px';
                }

                return this;
            }

        } )();

    } );

} )( window );
