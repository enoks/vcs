( function( $, window, document ) {

    $( document).on( 'ready', function() {

        var $form = $( '#post'),
            $schedule = $( 'table.season-schedule' ),
            $title = $( '#title' ),
            $titleDisplay = $( '<h2 id="titledisplay" />').html( $title.val() ),
            $year = $( '#season_year'),
            $league = $( '#season_league'),
            $teams = $('input[name="season_team[]"]');

        // hide title
        // will be set automatically
        $( '#titlediv' ).hide().after( $titleDisplay )
            .find( '#title' ).focus( function() {
                $( this ).blur();
            });

        // update title on change
        $year.on( 'change', function () {
            update_title();
            reload_league();
        } );
        $league.on( 'change', update_title );
        $teams.on( 'change', update_title );

        // ...
        function update_title() {
            var aTitle = [ $year[0].value, $league.find(':selected[value!=""]').text(), []],
                sTitle;

            // get team values
            //$teams.filter(':checked').each( function() {
            //    aTitle[2].push( $( this ).parent( 'label').text().trim() );
            //} );

            // remove empty
            aTitle = aTitle.filter( function( item ) {
                return item.length != undefined ? item.length : item;
            } );

            // 'stringify' arrays
            for ( var i = 0; i < aTitle.length; i++ ) {
                if ( Object.prototype.toString.call( aTitle[i] ) !== '[object Array]' ) continue;
                aTitle[i] = aTitle[i].join( ' + ' );
            }

            // build title
            sTitle = aTitle.join( ' - ' );

            // replace post title
            $title.val( sTitle );
            $titleDisplay.html( sTitle );
            // ... and slug
            $( '#post_name').val( sTitle.toLowerCase()
                .replace( new RegExp( '-', 'gi' ), '_' )
                .replace( new RegExp( '[/|+]', 'gi' ), '-')
                .replace( new RegExp( '[^a-z0-9-_]+|\\s+', 'gi' ), '' )
            );
        }

        /**
         * Datepicker
         */

        $.datepicker.setDefaults( $.datepicker.regional['de'] );

        function destroyDatepickers() {
            $( 'input[data-type="date"]', $schedule).each( function() {
                var $input = $( this );

                // destroy datepicker
                $input.datepicker('destroy');

                // remove datpicker id
                if ( $input.prop( 'id' ).match( new RegExp( '^dp\\d+' ) ) )
                    $input.prop( 'id', '' );
            } );
        }

        attachDatepicker();
        function attachDatepicker() {
            $( 'input[data-type="date"]', $schedule).each( function() {
                var $input = $( this );

                $input.datepicker( {
                    regional: 'de',
                    firstDay: 1,
                    dateFormat: 'dd.mm.yy'
                } )
            } );
        }

        /**
         * Autocomplete
         */

        attachAutocomplete();
        function attachAutocomplete() {
            $( 'input[data-autocomplete]').each( function() {
                var $input = $( this );

                if ( $input.attr( 'autocomplete' ) === 'off' ) return;

                $input.suggest( league.siteurl + '/wp-admin/admin-ajax.php?action=league-' + $( this).data( 'autocomplete' ) );
            } );
        }

        /**
         * Add/delete match
         */

        $form.on( 'click', 'a.add-row', function(e) {
            e.preventDefault();

            var $tbody = $( this).closest( 'table').find( 'tbody' ),
                $lastRow = $( 'tr:last', $tbody);

            if ( $lastRow.is(':first-child') && $lastRow.is( ':hidden' ) ) {
                $lastRow.show();
                return;
            }

            // destroy all datepickers
            destroyDatepickers();

            // duplicate last row
            var $match = $lastRow.clone();

            // clear data (except date)
            $match.find( 'input:not([type="date"])').removeAttr( 'autocomplete' ).val('');

            // add match
            $tbody.append( $match );

            attachDatepicker(); // re-attach datepicker
            attachAutocomplete(); // attach autocomplete to new fields
        } );

        $form.on( 'click', 'a.delete-row', function(e) {
            e.preventDefault();

            // get row to delete
            var $row = $( this ).closest( 'tr' );

            // hide available last match
            if ( $row.is( ':first-child' ) && $row.is( ':last-child' ) ) {
                $( ':input', $row).val( '' );
                $row.hide();
            }
            // remove match
            else $row.remove();
        });

        /**
         * Sort matches
         */

        $schedule.sortable({
            axis: 'y',
            containment: 'parent',
            items: 'tbody tr'
        });

        /**
         * Box reload.
         */

        function reload_league() {
            var oArgs = {
                    post: $( '#winning-sets' ).data( 'postid' ),
                    year: $year.val(),
                    league: $( '#season_league' ).val(),
                    bvv_msid: $( '#bvv-msid' ).val()
                },
                $inside = $( '#season_leaguediv').find( 'div.inside' ).addClass( 'loading' );

            emit( {
                'function': 'season_league__meta_box',
                args: oArgs
            }, function( data ) {
                $inside.html( data ).removeClass( 'loading' );
                reload_settings();
            } );
        }

        function reload_table() {
            var $input = $( '#winning-sets' ),
                oArgs = {
                    post: $input.data( 'postid' ),
                    winningsets: $input.val(),
                    manualtable: $( '#manual-table' ).is( ':checked' ),
                    bvv_msid: $( '#bvv-msid' ).val()
                },
                $inside = $( '#season_tablediv').find( 'div.inside' ).addClass( 'loading' );

            emit( {
                'function': 'season_table__meta_box',
                args: oArgs
            }, function( data ) {
                $inside.html( data ).removeClass( 'loading' );
                attachAutocomplete();
            } );
        }

        function reload_schedule() {
            var oArgs = {
                    post: $( '#winning-sets' ).data( 'postid' ),
                    bvv_msid: $( '#bvv-msid' ).val()
                },
                $inside = $( '#season_schedulediv').find( 'div.inside' ).addClass( 'loading' );

            emit( {
                'function': 'season_schedule__meta_box',
                args: oArgs
            }, function( data ) {
                $inside.html( data ).removeClass( 'loading' );
                $schedule = $( 'table.season-schedule' );
                attachAutocomplete();
                attachDatepicker();
            } );
        }

        function reload_settings() {
            var $input = $( '#winning-sets' ),
                oArgs = {
                    post: $input.data( 'postid' ),
                    winningsets: $input.val(),
                    manualtable: $( '#manual-table' ).is( ':checked' ),
                    bvv_msid: $( '#bvv-msid' ).val()
                },
                $inside = $( '#season_settingsdiv' ).find( 'div.inside' ).addClass( 'loading' );

            // reload settings
            emit( {
                'function': 'season_settings__meta_box',
                args: oArgs
            }, function( data ) {
                $inside.html( data).removeClass( 'loading' );
            } );

            // reload table
            reload_table();
        }

        $( document ).on( 'change', '#winning-sets', reload_settings );
        $( document ).on( 'change', '#bvv-msid', function() {
            reload_settings();
            reload_schedule();
        } );
        $( document ).on( 'change', '#manual-table', reload_table );

        // update note box
        $teams.on( 'change', function() {
            var oArgs = {
                    post: $( '#winning-sets' ).data( 'postid' ),
                    teams: []
                },
                $inside = $( '#season_notediv' ).find( 'div.inside' ).addClass( 'loading' );

            $teams.filter( ':checked' ).each( function() {
                oArgs.teams.push( $( this ).val() );
            } );
            oArgs.teams = oArgs.teams.join( ',' );

            // reload note box
            emit( {
                'function': 'season_note__meta_box',
                args: oArgs
            }, function( data ) {
                $inside.html( data ).removeClass( 'loading' );
            } );
        } );

        /**
         * Points input
         */

        $form.on( 'focus', 'input[name="points[]"]', function() {
            this.blur();

            var $match = $( this ).closest( 'tr' ),
                $points = $match.find( 'input[name="points[]"]'),
                $sets = $match.find( 'input[name="sets[]"]'),
                $teams = $match.find( 'input[name="team[]"]'),
                aPoints = [],
                sErrorClass = 'form-invalid';

            $points.each( function() {
                aPoints.push( this.value.split( '+' ) );
            } );

            // open modal
            tb_show(this.getAttribute( 'title' ), "#TB_inline?inlineId=points-input");

            // get result table in modal
            var $table = $( 'table', '#TB_ajaxContent'),
                $results = $( 'input', $table).val( '' );

            // teams
            $table.find( 'thead' ).find( 'th:nth-child(2)' ).html( $teams[0].value )
                .next().html( $teams[1].value );

            // points
            $.each( aPoints, function( i, team ) {
                $.each( team, function( j, points ) {
                    $( $results[j*2+i]).val( points );
                } );
            } );

            // 'submit'
            $table.find( 'button').off( 'click.results' ).on( 'click.results', function() {
                // input validation
                $results.removeClass( sErrorClass );

                var bError = false,
                    $resultsT1 = $results.filter( ':even'),
                    $resultsT2 = $results.filter( ':odd'),
                    aiSets = [0,0];

                $resultsT1.each( function(i) {
                    var $resultT1 = $( this ),
                        iResultT1 = $resultT1.val(),
                        $resultT2 = $( $resultsT2.get( i ) ),
                        iResultT2 = $resultT2.val();

                    if ( iResultT1 && !$.isNumeric( iResultT1 ) ) {
                        $resultT1.addClass( sErrorClass );
                        bError = true;
                    }

                    if ( iResultT2 && !$.isNumeric( iResultT2 ) ) {
                        $resultT2.addClass( sErrorClass );
                        bError = true;
                    }

                    if ( iResultT1 && !iResultT2 ) {
                        $resultT2.addClass( sErrorClass );
                        bError = true;
                    }
                    else if ( !iResultT1 && iResultT2 ) {
                        $resultT1.addClass( sErrorClass );
                        bError = true;
                    }
                    else if ( iResultT1 && iResultT2 ) {
                        // sets
                        if ( parseInt( iResultT1 ) > parseInt( iResultT2 ) ) aiSets[0]++;
                        else aiSets[1]++;
                    }
                } );

                if ( bError ) return;

                // ...
                $points.each( function( i ) {
                    $( this ).val( $results.filter( !i ? ':even' : ':odd' )
                        .map( function() { return this.value; } )
                        .get().filter( function( item ) { return item; } ).join( '+' ) );
                } );

                $sets.each( function(i) {
                    $( this ).val( aiSets[i] );
                } );

                tb_remove();
            } );
        } );

        /**
         * Form validation.
         */

        $form.submit( function() {
            var $form = $( this),
                sErrorClass = 'form-invalid',
                $required = $( '.required:input', $form),
                $numeric = $( '.numeric:input', $form),
                bValid = true;

            // reset
            $( '.form-invalid', $form ).removeClass( sErrorClass );

            // check required fields
            $required.each( function() {
                var $input = $( this );
                if ( $input.val() != '' ) return;

                // tweak for 'empty' schedule
                if ( $input.closest( 'table.season-schedule').length
                    && $input.closest( 'tr').is( ':hidden' ) ) return;

                $input.addClass( sErrorClass )
                    .closest( 'div.postbox').removeClass( 'closed' );

                bValid = false;
            } );

            // check numeric fields
            $numeric.each( function() {
                var $input = $( this ),
                    inputValue = $input.val();

                if ( !inputValue || $.isNumeric( inputValue ) ) return;

                // tweak for 'empty' schedule
                if ( $input.closest( 'table.season-schedule').length
                    && $input.closest( 'tr').is( ':hidden' ) ) return;

                $input.addClass( sErrorClass )
                    .closest( 'div.postbox').removeClass( 'closed' );

                bValid = false;
            } );

            // validate schedule
            $( 'table.season-schedule tbody tr:not(:hidden)', $form).each( function() {
                var $row = $( this ),
                    aFields = [
                        $( 'input[name="points[]"]', $row ),
                        $( 'input[name="sets[]"]', $row )
                    ],
                    $firstField, firstValue, $secondField, secondValue;

                for ( var i = 0; i < aFields.length; i++ ) {
                    $firstField = $( aFields[i][0] );
                    firstValue = $firstField.val();
                    $secondField = $( aFields[i][1] );
                    secondValue = $secondField.val();

                    if ( ( firstValue == '' && secondValue == '' )
                        || ( firstValue != '' && secondValue != '' ) ) continue;
                    else if ( firstValue == '' ) $firstField.addClass( sErrorClass )
                        .closest( 'div.postbox').removeClass( 'closed' );
                    else $secondField.addClass( sErrorClass )
                            .closest( 'div.postbox').removeClass( 'closed' );

                    bValid = false;
                }
            } );

            // error occurred
            if ( !bValid ) return false;
        } );

    } );

} )( jQuery, window, window.document );
