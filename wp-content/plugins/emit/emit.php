<?php

/**
 * Plugin Name: Emit
 * Plugin URI: http://www.artcomventure.de/
 * Description: Get page, posts, etc. content through ajax calls.
 * Version: 1.0.0
 * Author: Stefan Käsche
 * Author URI: http://www.artcomventure.de/
 */

/**
 * Enqueue scripts and styles.
 */
add_action( 'wp_enqueue_scripts', 'emit__wp_enqueue_scripts' );
function emit__wp_enqueue_scripts() {
    wp_enqueue_script( 'emit', plugins_url( '/emit.js', __FILE__ ), array(), '20130629', true );
}

add_action( 'admin_enqueue_scripts', 'emit__admin_enqueue_scripts', 10, 1 );
function emit__admin_enqueue_scripts( $hook ) {
  wp_enqueue_script( 'emit', plugins_url( '/emit.js', __FILE__ ), array(), '20130629', true );
}

// ...
add_action( 'wp_ajax_emit', 'emit' );
add_action( 'wp_ajax_nopriv_emit', 'emit' );
function emit() {
    $atts = $_GET + array(
        'action' => '',
        'function' => '',
        'args' => array()
    );

    $return = null;
    if ( $atts['action'] == 'emit' ) {
        if ( function_exists( $atts['function'] ) ) {
            $return = call_user_func_array( $atts['function'], $atts['args'] );
        }
    }

    // return as json
    if ( is_object( $return ) || is_array( $return ) )
        $return = json_encode( $return );

    echo $return;
    wp_die();
}

function get_content_by_id( $id ) {
    if ( empty( $id ) ) return null;

    // get post
    $content = get_post( $id );
    // 'render' post
    $content = apply_filters( 'the_content', $content->post_content );

    return str_replace( ']]>', ']]&gt;', $content );
}
