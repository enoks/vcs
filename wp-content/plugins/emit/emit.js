( function( window, undefined ) {

    window.emit = function( oParams, ofCallbacks ) {
        var xhr = new XMLHttpRequest(),
            aParams, sProp, sParams = '';

        // convert params to object
        if ( oParams == undefined ) oParams = {};
        if ( typeof oParams === 'string' ) {
            aParams = oParams.split( '&' );
            oParams = {};

            for ( var i = 0; i < aParams.length; i++ ) {
                aParams[i] = aParams[i].split( '=' );
                oParams[aParams[i][0]] = aParams[i][1];
            }
        }

        // add trigger param for WP
        oParams.action = 'emit';

        // object of params back to string
        for ( sProp in oParams ) {
            if ( oParams.hasOwnProperty( sProp ) ) {
                if ( typeof oParams[sProp] === 'object' ) {
                    for ( var key in oParams[sProp] ) {
                        sParams += ( sParams ? '&' : '' ) + sProp + '[' + key + ']=' + oParams[sProp][key];
                    }
                }
                else sParams += ( sParams ? '&' : '' ) + sProp + '=' + oParams[sProp];
            }
        }

        if ( ofCallbacks == undefined ) ofCallbacks = {};
        else if ( typeof ofCallbacks === 'function' ) ofCallbacks = { success: ofCallbacks };

        xhr.open( 'GET', wp.ajax.settings.url + ( sParams ? '?' + sParams : '' ) );

        xhr.onreadystatechange = function() {
            if ( xhr.readyState === 4 ) {
                // everything is fine
                if (xhr.status === 200 || xhr.status === 0) ofCallbacks.success && ofCallbacks.success( xhr.responseText );
                // whoops. something went wrong
                else ofCallbacks.error && ofCallbacks.error( xhr.responseText );

                // done
                ofCallbacks.complete && ofCallbacks.complete();
            }
        };

        // action before sending the request
        ofCallbacks.beforeSend && ofCallbacks.beforeSend();

        // eventually send request
        xhr.send();
    }

} )( window );
