var gulp = require( 'gulp' ),

    // gulp plugins

    sass = require( 'gulp-sass' ),
    sourcemaps = require( 'gulp-sourcemaps' ),
    // css vendor prefixes
    autoprefixer = require( 'gulp-autoprefixer' ),
    rename = require( 'gulp-rename' ),
    // minimize css
    cssnano = require( 'gulp-cssnano' ),
    // uglify (and minimize) js
    uglify = require( 'gulp-uglify' ),
    // beautify css
    csscomb = require( 'gulp-csscomb' ),
    replace = require( 'gulp-replace' ),

    gettext = require( 'gulp-gettext' ),

    // doesn't break pipe on error
    // so we don't need to restart gulp
    plumber = require( 'gulp-plumber' ),
    // get notification on error
    notify = require( 'gulp-notify' ),
    onError = function( error ) {
        notify.onError( {
            title:    'Gulp Failure',
            //subtitle: '... in ' + error.message.split( "\n" )[0],
            message:  '<%= error.message %>',
            sound:    'Beep'
        } )( error );

        this.emit( 'end' );
    },

    scssFiles = [
        'wp-content/themes/siehste/**/*.scss',
        'wp-content/plugins/league/**/*.scss'
    ],

    cssFiles = [
        'wp-content/themes/siehste/**/*.css',
        'wp-content/plugins/league/**/*.css',
        '!**/*.min.css'
    ],

    jsFiles = [
        'wp-content/themes/siehste/**/*.js',
        'wp-content/plugins/league/**/*.js',
        'wp-content/plugins/emit/**/*.js',
        '!**/*.min.js'
    ];

/**
 * Compile scss to css and create sourcemap.
 */
gulp.task( 'scss', function() {
    return gulp.src( scssFiles, { base: 'wp-content' } )
        .pipe( plumber( { errorHandler: onError } ) )
        .pipe( sourcemaps.init() )
        .pipe( sass() )
        .pipe( autoprefixer( {
            browsers: ['last 3 versions']
        } ) )
        .pipe(csscomb())
        // add a blank line between two instructions
        .pipe(replace(/}\n(\.|#|\w|\s*\d)/g, "}\n\n$1"))
        // remove blank lines in instruction
        .pipe(replace(/;\s*\n(\s*\n)+/g, ";\n"))
        // remove comment blocks
        //.pipe(replace(/\/\*[^\!]([\s\S]+?)\*\/(\n)?/g, ''))
        .pipe( sourcemaps.write( '.' ) )
        .pipe( gulp.dest( 'wp-content' ) );
} );

/**
 * Compress css files.
 */
gulp.task( 'css', ['scss'], function() {
    gulp.src( cssFiles, { base: 'wp-content' } )
        .pipe( plumber( { errorHandler: onError } ) )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( cssnano() )
        .pipe( gulp.dest( 'wp-content' ) );
} );

/**
 * Compress and uglify js files.
 */
gulp.task( 'js', function() {
    gulp.src( jsFiles, { base: 'wp-content' } )
        .pipe( plumber( { errorHandler: onError } ) )
        .pipe( rename( { suffix: '.min' } ) )
        .pipe( uglify() )
        .pipe( gulp.dest( 'wp-content' ) );
} );

// compile .po files to .mo
var poFiles = [
    'wp-content/themes/siehste/languages/*.po',
    'wp-content/plugins/league/languages/*.po'
];
gulp.task( 'po2mo', () => gulp.src( poFiles )
    .pipe( gettext() )
    .pipe( gulp.dest( function (file) {
        return file.base;
    } ) )
);

// watch task
gulp.task( 'default', ['css', 'js', 'po2mo'], function() {
    gulp.watch( scssFiles, ['css'] );
    gulp.watch( jsFiles, ['js'] );
    gulp.watch( poFiles, ['po2mo'] );
} );
