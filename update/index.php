<?php

header('Content-Encoding: none;');

class Update
{
    protected $_username = '';
    protected $_password = '';

    protected $_url = 'https://bitbucket.org/enoks/vcs/get/';
    protected $_file = 'zip';
    protected $_destination = '';
    // folder from zip
    protected $_folder = '';

    protected $_fp = null;
    protected $_ch = null;

    protected $_runthrough = FALSE;

    function __construct( $username = '', $password = '', $runthrough = TRUE, $branchname = 'master' )
    {
        // echo status
        $this->progress( 'Initialize' );

        if ( !$username || !$password ) {
            $errMsg = 'Missing username or password.';
            $this->progress( 'ERROR<br />' . $errMsg, 'error' );
            throw new Exception( $errMsg, 500 );
        }

        $this->_username = $username;
        $this->_password = $password;

        // build urls and destinations
        $this->_file = ( $branchname ? $branchname : 'master' ) . '.' . $this->_file;
        $this->_url .= $this->_file;
        $this->_destination .= dirname( __FILE__ ) . '/../../' . $this->_file;

        // echo status
        $this->progress( 'OK', 'success' );

        // call next step
        if ( $runthrough ) {
            $this->_runthrough = $runthrough;
            return $this->download();
        }

        return $this;
    }

    /**
     * Echoing messages.
     *
     * @param string $message
     * @param string $type
     * @return $this|void
     */
    public function progress( $message = '', $type = 'task' )
    {
        if ( !$message ) return;

        echo '<span class="' . $type . '">';
        echo str_pad( $message, 4096*2 );
        echo '</span>';

        return $this;
    }

    /**
     * Login to Bitbucket and download zip file from repository.
     *
     * @return $this|Update|void
     * @throws Exception
     */
    protected function download()
    {
        // echo status
        $this->progress( 'Downloading repository from <a href="https://bitbucket.org/enoks/vcs">Bitbucket</a>' );

        try {
            $this->_fp = fopen( $this->_destination, "w" );
            $this->_ch = curl_init();
            curl_setopt( $this->_ch, CURLOPT_URL, $this->_url );
            curl_setopt( $this->_ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC );
            curl_setopt( $this->_ch, CURLOPT_USERPWD, $this->_username . ":" . $this->_password );
            curl_setopt( $this->_ch, CURLOPT_FILE, $this->_fp );
            curl_setopt( $this->_ch, CURLOPT_SSL_VERIFYPEER, false );
//            curl_setopt( $this->_ch, CURLOPT_FOLLOWLOCATION, true );
            $result = curl_exec( $this->_ch );

            // validate CURL status
            if( curl_errno( $this->_ch) ) {
                throw new Exception( curl_error( $this->_ch ), 500 );
            }

            // validate HTTP status code (user/password credential issues)
            $status_code = curl_getinfo( $this->_ch, CURLINFO_HTTP_CODE );
            if ( $status_code != 200 ) {
                throw new Exception( "Response with Status Code [" . $status_code . "].", 500 );
            }
        }
        catch( Exception $e ) {
            $this->close();

            // echo status
            $this->progress( 'ERROR', 'error' );
            $this->progress( '<br /><a href="./">Go back</a> and try again.', '' );

            throw new Exception( 'Unable to properly download file from url=[' . $this->_url . '] to path [' . $this->_destination . '].', 500, $e );
        }

        $this->close();

        // echo status
        $this->progress( 'OK', 'success' );

        // call next step
        if ( $this->_runthrough ) {
            return $this->unzip();
        }

        return $this;
    }

    /**
     * Extract zip file.
     *
     * @return $this|Update|void
     * @throws Exception
     */
    protected function unzip()
    {
        // echo status
        $this->progress( 'Unpacking' );

        $zip = new ZipArchive;
        if ( $zip->open( "../../$this->_file" ) === TRUE) {
            $this->_folder = explode('/', $zip->getNameIndex( 0 ) );
            $this->_folder = $this->_folder[0];
//            $zip->extractTo( '../..' );
            $zip->close();

            // use WPs unzip
            require( '../wp-load.php' );
            require_once( dirname( __FILE__ ) . '/../wp-admin/includes/file.php' );
            WP_Filesystem();
            unzip_file( "../../$this->_file", "../.."  );

            // echo status
            $this->progress( 'OK', 'success' );

            // call next step
            if ( $this->_runthrough ) {
                return $this->copyFiles();
            }
        }
        else {
            // echo status
            $this->progress( "ERROR", 'error' );
            throw new Exception( 'Unable to unpack zip.' );
        }

        return $this;
    }

    /**
     * Copy/move not versioned files and folders (wp.config.php, upload/*).
     *
     * @return $this|void
     * @throws Exception
     */
    protected function copyFiles()
    {
        // echo status
        $this->progress( "Copying not versioned files" );

        try {
            copy( '../wp-config.php', "../../{$this->_folder}/wp-config.php" );
            foreach ( array( '.idea', 'wp-content/uploads' ) as $folder ) {
                if ( file_exists( "../{$folder}/" ) ) {
                    rename( "../{$folder}/", "../../{$this->_folder}/{$folder}/" );
                }
            }
        }
        catch (Exception $e) {
            // echo status
            $this->progress( 'ERROR', 'error' );
            throw new Exception();
        }

        // echo status
        $this->progress( 'OK', 'success' );

        // call next step
        if ( $this->_runthrough ) {
            return $this->release();
        }

        return $this;
    }

    /**
     * Release new version.
     */
    protected function release()
    {
        // echo status
        $this->progress( 'Release new version' );

        try {
            rename( "../../vcs/", '../../vcs_' . time() . '/' );
            rename( "../../{$this->_folder}/", '../../vcs/' );
        }
        catch (Exception $e) {
            // echo status
            $this->progress( 'ERROR', 'error' );
            throw new Exception( 'Unable to rename root folder.' );
        }

        unlink( $this->_destination );

        // echo status
        $this->progress( 'OK', 'success' );
    }

    protected function close()
    {
        if ( $this->_ch != null ) curl_close( $this->_ch );
        if ( $this->_fp != null ) fclose( $this->_fp );
    }
}

?>
<!DOCTYPE html>

<head>
    <title>Update VCS</title>
</head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" media="all" href="./../wp-admin/css/login.min.css" />
<link rel="stylesheet" media="all" href="./../wp-includes/css/buttons.min.css" />
<style type="text/css">
    @import url(http://fonts.googleapis.com/css?family=Open+Sans:400,700);

    html {
        display: table;
        width: 100%;
    }

    body {
        display: table-cell;
        width: 100%;
        height: 100vh;
        vertical-align: middle;
        text-align: center;
    }

    body > * {
        text-align: left;
    }

    form, #login_error, h1,
    body > p {
        width: 320px;
        display: block;
        margin: 20px auto !important;
        -moz-box-sizing: border-box;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
    }

    h2 {
        margin-bottom: 1em !important;
    }

    .login form p {
        margin: 0 0 1.5em;
    }

    code {
        position: absolute;
        top: 10px;
        right: 10px;
        left: 10px;
    }

    code span.task:after {
        content: '...';
        padding-right: 0.56em;
        display: inline-block;
        overflow: hidden;
        height: 1em;
        position: relative;
        top: 1px;
        width: 20px;
    }

    code span:last-child:after {
        -moz-animation: pending 0.5s linear infinite;
        -webkit-animation: pending 0.5s linear infinite;
        animation: pending 0.5s linear infinite;
    }

    @-moz-keyframes pending {
        0% { width: 0; }
        33% { width: 10px; }
        66% { width: 20px; }
    }

    @-webkit-keyframes pending {
        0% { width: 0; }
        33% { width: 7px; }
        66% { width: 15px; }
    }

    @keyframes pending {
        0% { width: 0; }
        33% { width: 10px; }
        66% { width: 20px; }
    }

    code span.success {
        color: green;
    }

    code span.success:after,
    code span.error:after {
        content: ".";
        clear: both;
        display: block;
        visibility: hidden;
        height: 0;
    }

    code span.error {
        color: red;
    }
</style>

<body class="login wp-core-ui">

<?php
if ( !empty( $_POST['username'] ) && !empty( $_POST['password'] ) ) :

    echo '<code>';
    try {
        $update = new Update( $_POST['username'], $_POST['password'], TRUE, $_POST['branchname'] );
        $update->progress( '<br />Done. <a href="../">Back to VCS website.</a>', 'success' );
    }
    catch (Exception $e) {}
    echo '</code>';

else : ?>

    <h1>
        <img width="100" height="100" src="../wp-content/themes/siehste/images/logo.png" alt="VCS Logo" title=""/>
    </h1>

    <?php if ( !empty( $_POST ) && ( empty( $_POST['username'] ) || empty( $_POST['password'] ) ) ) : ?>
    <div id="login_error">
        <strong>Fehler:</strong> Login-Daten überprüfen.<br>
    </div>
    <?php endif; ?>

    <form method="post" action="">
        <h2>Update VC Strausberg e.V.</h2>
        <p>Deine <a href="https://bitbucket.org/enoks/vcs" target="_blank">Bitbucket</a> Login-Daten eintragen,
            <i>Update starten</i> klicken und ... das war's.</p>
        <div>
            <label for="username">Benutzername</label>
            <input type="text" name="username" class="input"
                   value="<?php echo ( !empty( $_POST['username'] ) ? $_POST['username'] : '' ); ?>" />
        </div>
        <div>
            <label for="password">Passwort</label>
            <input type="password" name="password" class="input"
                   value="<?php echo ( !empty( $_POST['password'] ) ? $_POST['password'] : '' ); ?>" />
        </div>
        <div style="display:none;">
            <label for="branchname">Branchname</label>
            <input type="text" name="branchname" class="input" placeholder="tip"
                   value="<?php echo ( !empty( $_POST['branchname'] ) ? $_POST['branchname'] : '' ); ?>" />
        </div>
        <button class="button-primary">Update starten</button>
    </form>

    <p id="backtoblog">
        <a href="../">&larr; Zurück zu Volleyballclub Strausberg e.V.</a>
    </p>

<?php endif; ?>

</body>

</html>